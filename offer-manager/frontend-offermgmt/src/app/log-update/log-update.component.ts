import { Component,OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import {Login} from '../classes/login';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-log-update',
  templateUrl: './log-update.component.html',
  styleUrls: ['./log-update.component.css']
})
export class LogUpdateComponent implements OnInit{
  
    login:Login;
    oldLogin:Login; 
    trigramme:any="";
    oldMdpuserByForm:string="";
  
  constructor( private _freeApiService: freeApiService,private authService: AuthService,private router: Router) {
    this.login=new Login(); this.oldLogin=new Login();
  }
  
  ngOnInit(): void {
    //get login  by trigramme
    this.trigramme = this.authService.trigrammeService;

    this._freeApiService.GetOneLoginById(this.trigramme).subscribe
    (
      data=> {
        //Get old Login
        this.oldLogin = data;

        //Can change if needed
        this.login.depuser = this.oldLogin.depuser;
        this.login.trigramme = this.oldLogin.trigramme;

        console.log(this.oldLogin);
      }
  );
    
  }
  
    //--------UPDATE COMPTE-----------/
  
  /* onSubmit(form:NgForm):void{
    console.log(this.oldLogin);
    console.log(this.login);
    console.log(this.oldMdpuserByForm);

    if(this.oldMdpuserByForm == this.oldLogin.mdpuser){
      this._freeApiService.UpdateLogin(this.login).subscribe(
        resp=>{
            console.log(resp);
            console.log("Eny");
              
            this.login = resp;
    
            Swal.fire({
              position: 'center',
              icon: 'success',
              text: 'La mise à jour a été effectuée',
              showConfirmButton: false,
              timer: 3000
            })

            if(this.login.gpuser=="M"){
              console.log("Tonga ato am M?");
              this.router.navigate(['Marketing-offres']);
            }
            else if(this.login.gpuser=="T"){
              console.log("Tonga ato am T?");
              this.router.navigate(['Technique-offres']);
            }
            else if(this.login.gpuser=="A"){
              console.log("Tonga ato am A?");
              this.router.navigate(['Admin-login']);
            }
          
           },
        error1=>{
            Swal.fire({
              position: 'center',
              icon: 'error',
              text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
              showConfirmButton: false,
              timer: 3000
            })
        } 

       );
    }
    else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'Désolé, le mot de passe actuel que vous venez de saisir n\'est pas correct. Veuillez réessayer à nouveau',
        showConfirmButton: false,
        timer: 5000
      })
    }

       
  } */

/*   annuler():void{
    if(this.login.gpuser=="M"){
      console.log("Tonga ato am M?");
      this.router.navigate(['Marketing-offres']);
    }
    else if(this.login.gpuser=="T"){
      console.log("Tonga ato am T?");
      this.router.navigate(['Technique-offres']);
    }
    else if(this.login.gpuser=="A"){
      console.log("Tonga ato am A?");
      this.router.navigate(['Admin-login']);
    }
  } */

  /* afficheMdpuser(e: any) {
    if(e.target.checked){
      (<HTMLInputElement>document.getElementById("mdpuser")).type = "text";
      (<HTMLInputElement>document.getElementById("oldMdpuserByForm")).type = "text";
    }
    else{
      (<HTMLInputElement>document.getElementById("mdpuser")).type = "password";
      (<HTMLInputElement>document.getElementById("oldMdpuserByForm")).type = "password";
    }
	  
	} */
  
}
