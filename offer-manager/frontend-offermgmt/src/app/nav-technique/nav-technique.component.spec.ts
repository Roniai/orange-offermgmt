import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavTechniqueComponent } from './nav-technique.component';

describe('NavTechniqueComponent', () => {
  let component: NavTechniqueComponent;
  let fixture: ComponentFixture<NavTechniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavTechniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavTechniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
