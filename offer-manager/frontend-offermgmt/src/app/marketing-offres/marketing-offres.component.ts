import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Bundle} from '../classes/bundle';
import { BundleOfferelementaire } from '../classes/bundleofferelementaire';
import { Offerelementaire } from '../classes/offerelementaire';


@Component({
  selector: 'app-marketing-offres',
  templateUrl: './marketing-offres.component.html',
  styleUrls: ['./marketing-offres.component.css']
})
export class MarketingOffresComponent implements OnInit {

  bundle:Bundle= new Bundle();
  lstBundle :Bundle[];
  data: any = {};

  /* Profilebundle */
  lstprofile:any[];
  listeSelected: any[] = [];
  listIdprofile:any[] = [];
  mylstprofileObject:any = {};

  /* Bundleofferelementaire */
  lstBundleofferelementaire:BundleOfferelementaire[];
  offerelementaire:Offerelementaire = new Offerelementaire();
  lstnature:[]=[];
    /* BundleofferelementaireToUpdate */
  mylstnature:any[]=[];
  mylstquantity:any[]=[];
  mylstunit:any[]=[];
  mylstDA:any[]=[];
  mylstnatureObject:any = {};  

  listeSelectedNature: any[] = [];
  listDA:any[]=[];
  listUnit:any[]=[];
  listQuantity:any[]=[];
  listDAquantity: any = {};

  lstDAToUpdate:any[]=[];
  lstDAToDelete:any[]=[];
  listDAquantityToUpdate: any = {};
  listDAquantityToInsert: any = {};

  /* Checkbox */
  lstIsObject:any = {};


  constructor( private _freeApiService: freeApiService) { 
    this.bundle = new Bundle();this.lstBundle=[]; this.lstprofile=[];
    this.lstBundleofferelementaire = [];}

  ngOnInit(): void {
    this.bundle = new Bundle();
    this.listeSelected=[];

    ///get
    this._freeApiService. getBundlemk().subscribe
    (
      data=> {
        this.lstBundle = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );
    this._freeApiService. getNature().subscribe
    (
      data=> {
        this.lstnature = data;
      }
    );
  }


    //Put(maka value fotsiny )
    view(data:Bundle):void{
      this.bundle=data;

      this.lstIsObject["isbuylimited"] = this.bundle.isbuylimited;
      this.lstIsObject["isBuyableForOther"] = this.bundle.isBuyableForOther;
      this.lstIsObject["isBuyableWithLanyCredit"] = this.bundle.isBuyableWithLanyCredit;
      this.lstIsObject["isBuyableByOM"] = this.bundle.isBuyableByOM;
      this.lstIsObject["isautorenewable"] = this.bundle.isautorenewable;
      
      //Profilebundle
      this._freeApiService.GetIdprofileofBundle(data.idbundle).subscribe(
        resp=>{
          this.listIdprofile = resp;

          //Initialisation
          this.listeSelected=[];
          this.mylstprofileObject={};

          for (let i = 0; i < this.lstprofile.length ; i++){
            //Si l'élément existe déjà
            if(this.listIdprofile.indexOf(this.lstprofile[i])!=-1){
                //Tableau associatif pour stocker "checked=true or false"
                this.mylstprofileObject[this.lstprofile[i]] = true;

                this.listeSelected.push(this.lstprofile[i]);

            }
            else{
              this.mylstprofileObject[this.lstprofile[i]] = false;
            }
    
          }

          console.log("this.mylstprofileObject : ",this.mylstprofileObject);

        });

      //Bundleofferelemenataire
      console.log("data idbundle = ",data.idbundle);
      this._freeApiService.getBundleofferelementaireByIdbundle(data.idbundle).subscribe(
        resp=>{
          /* console.log(resp); */
          this.lstBundleofferelementaire = resp;
          console.log("lstBundleofferelementaire = ",this.lstBundleofferelementaire);
            
            //Initialisation
            let that = this;
            that.mylstnature=[];
            that.mylstunit=[];
            that.mylstDA=[];
            that.mylstquantity=[];
            that.mylstnatureObject={};
            that.listeSelectedNature=[];
            that.listUnit=[];
            that.listDA=[];
            that.listQuantity=[];

            let myPromise = new Promise(function(myResolve, myReject) {
              for (let i = 0; i < that.lstBundleofferelementaire.length ; i++){

                that._freeApiService.GetOneOfferelementaire(that.lstBundleofferelementaire[i].DA).subscribe(res => {
                  that.offerelementaire = res;
                  console.log("offerelementaire = ",that.offerelementaire);

                  that.mylstnature.push(that.offerelementaire.nature);
                  that.mylstunit.push(that.offerelementaire.unit);
                  that.mylstquantity.push(that.lstBundleofferelementaire[i].quantity);
                  that.mylstDA.push(that.lstBundleofferelementaire[i].DA);


                  //Appel la fonction resolve quand cette boucle est terminée
                  if(that.mylstnature.length == that.lstBundleofferelementaire.length) {
                    myResolve("Function completed");
                  }
                
                });  
                
              }
              //Pas encore de offerelementaire
              if(that.lstBundleofferelementaire.length==0) {
                myResolve("Function completed");
              }
              
            });
            
            myPromise.then(function(value) {
              /* console.log(value); */
              console.log("nature ivelany= ",that.mylstnature);
              console.log("unit ivelany= ",that.mylstunit);
              console.log("DA ivelany= ",that.mylstDA);
              console.log("quantity ivelany= ",that.mylstquantity);

              for (let i = 0; i < that.lstnature.length ; i++){
  
                //Si l'élément existe déjà
                if(that.mylstnature.indexOf(that.lstnature[i])!=-1){
                  /* ########## TD LEFT ########## */

                    //Tableau associatif pour stocker "checked=true or false"
                    that.mylstnatureObject[that.lstnature[i]] = true;

                  /* ########## TD RIGHT ########## */

                    /* Nature selected */
                    that.listeSelectedNature.push(that.lstnature[i]);

                    let indexOf = that.mylstnature.indexOf(that.lstnature[i]);

                      /* DA */
                      that.listDA.push(that.mylstDA[indexOf]);

                      /* Quantity */
                      that.listQuantity.push(that.mylstquantity[indexOf]);

                      /* Unit */  

                      if(that.mylstunit[indexOf]==1){
                        that.listUnit.push("SMS");
                      }
                      else if(that.mylstunit[indexOf]==2){
                        that.listUnit.push("Ariary");
                      }
                      else if(that.mylstunit[indexOf]==3){
                        that.listUnit.push("Seconde");
                      }
                      else if(that.mylstunit[indexOf]==4){
                        that.listUnit.push("Octet");
                      }
                      else{
                        that.listUnit.push("Unité inconnue");
                      }

                }
                else{
                  that.mylstnatureObject[that.lstnature[i]] = false;
                }

              }
             /*  console.log('FDZ : ' , that.mylstnatureObject); */
              console.log('Selected nature : ' , that.listeSelectedNature);
              console.log('DA : ' , that.listDA);
              console.log('Unit : ' , that.listUnit);
              console.log('Quantité : ' , that.listQuantity);


            });

        });
    }
    
  
  }
