import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingOffresComponent } from './marketing-offres.component';

describe('MarketingOffresComponent', () => {
  let component: MarketingOffresComponent;
  let fixture: ComponentFixture<MarketingOffresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketingOffresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingOffresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
