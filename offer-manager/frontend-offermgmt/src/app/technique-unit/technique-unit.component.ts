import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { Unit } from '../classes/unit';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-technique-unit',
  templateUrl: './technique-unit.component.html',
  styleUrls: ['./technique-unit.component.css']
})
export class TechniqueUnitComponent implements OnInit {
  roleuser:any="";
  
  lstUnit :Unit[];
  unit:Unit= new Unit();
  isEdit:boolean=false;
  
  
  constructor( private _freeApiService: freeApiService,private authService: AuthService, private router: Router ) { this.lstUnit=[]; }

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;

    ///get
    this._freeApiService.getUnit().subscribe
    (
      data=> {
        this.lstUnit = data;
      }
    );
  }

  
 onSubmit(form:NgForm):void{
  if(!this.isEdit){
  this._freeApiService.CreateUnit(form.value).subscribe(
    resp=>{
      console.log(resp);
      
      this.closeModale();
      
      Swal.fire({
        position: 'center',
        icon: 'success',
        text: 'La nouvelle unité est créée avec succès',
        showConfirmButton: false,
        timer: 2500
      })
   
       },
     error1=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
        showConfirmButton: false,
        timer: 2500
      })
 
    
    }
   
    
    
   );
   
  }
  //PUt
     else{
       console.log(form.value)
      this._freeApiService.UpdateUnit(this.unit).subscribe(
        resp=>{
          console.log(resp);
         
          this.closeModale();

          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'La mise à jour de l\'unité "'+this.unit.description+'" a été bien effectuée',
            showConfirmButton: false,
            timer: 2500
          })
      
          },
        error1=>{
    
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
            showConfirmButton: false,
            timer: 2500
          })
        }
        
      )}

  
  }

  

  add():void{
    this.isEdit=false;
    this.unit=new Unit;
  }

  //Put(maka value fotsiny )
  edit(data:Unit):void{
    this.isEdit=true;
    this.unit=data;
  }

  //delete
  delete(data:Unit):void{
    
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Annuler',
      cancelButtonColor: 'black',
      confirmButtonText: 'Oui, supprime-le!',
      confirmButtonColor: '#ff6600' 

    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'L\'unité "'+data.description+'" a été supprimé définitivement',
          showConfirmButton: false,
          timer: 2500
         
        })
        this._freeApiService.DeleteUnit(data.unit).subscribe(
          resp=>{
            this.ngOnInit();
            
          }
        )
      }
    })

  }

  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  closeModale(){
    //Fermer modale
    let x = document.getElementById('id01');
    if(x!=null){x.style.display='none'}
    this.ngOnInit();
  }

}
