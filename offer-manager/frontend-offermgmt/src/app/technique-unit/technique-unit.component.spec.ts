import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueUnitComponent } from './technique-unit.component';

describe('TechniqueUnitComponent', () => {
  let component: TechniqueUnitComponent;
  let fixture: ComponentFixture<TechniqueUnitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueUnitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
