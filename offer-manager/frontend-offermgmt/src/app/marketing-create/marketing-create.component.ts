import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import {Login} from '../classes/login';
import { Bundle} from '../classes/bundle';
import { Profile } from '../classes/profile';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-marketing-create',
  templateUrl: './marketing-create.component.html',
  styleUrls: ['./marketing-create.component.css']
})
export class MarketingCreateComponent implements OnInit {
  roleuser:any="";
  depuser:any="";

  bundle:Bundle;
  isEdit:boolean=false;

  /* Profilebundle */
  lstprofile:Profile[];
  listeSelected: any[] = [];

  /* Bundleofferelementaire */
  lstnature:[]=[];
  listeSelectedNature: any[] = [];
  listDA:any[]=[];
  listUnit:any[]=[];
  listQuantity:any[]=[];
  listDAquantity: any = {};

  /* Checkbox */
  lstIsObject:any = {};

  /*List Groups*/ 
  lstGroups:[]=[];
  lstGroups1:[]=[];
  lstGroups2:[]=[];
  lstGroups3:[]=[];
  lstGroups4:[]=[];
  lstGroups5:[]=[];

  constructor( private _freeApiService: freeApiService,private authService: AuthService) {this.bundle=new Bundle(); this.lstprofile=[];}

  ngOnInit(): void {
    //########## get login ##########
    this.roleuser=this.authService.roleuserService;
    this.depuser=this.authService.depuserService;



    //Initialiser bundle, listeSelected, listDA, listQuantity
    this.bundle=new Bundle();
    console.log('Bundle ngOninit: ',this.bundle);

    //Taxe = TTC par défaut
    this.bundle.taxe="TTC";

    this.listeSelected=[];
    console.log('listeSelected ngOninit: ',this.listeSelected);

    this.listeSelectedNature = [];
    this.listQuantity = [];
    this.listDA = [];
    this.listUnit = [];
    this.listDAquantity = {};

    //Checkbox is
    this.lstIsObject["isbuylimited"] = 0;
    this.lstIsObject["isBuyableForOther"] = 0;
    this.lstIsObject["isBuyableWithLanyCredit"] = 0;
    this.lstIsObject["isBuyableByOM"] = 0;
    this.lstIsObject["isautorenewable"] = 0;

    this.isBuyLimitedChecked(false);

    //get
    this._freeApiService. getProfile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );
    this._freeApiService. getNature().subscribe
    (
      data=> {
        this.lstnature = data;
      }
    );

    /*List Groups*/
    this._freeApiService. getGroups().subscribe
    (
      data=> {
        this.lstGroups = data;
        console.log(this.lstGroups);
      }
    );
    this._freeApiService. getGroups1().subscribe
    (
      data=> {
        this.lstGroups1 = data;
        console.log(this.lstGroups1);
      }
    );
    this._freeApiService. getGroups2().subscribe
    (
      data=> {
        this.lstGroups2 = data;
        console.log(this.lstGroups2);
      }
    );
    this._freeApiService. getGroups3().subscribe
    (
      data=> {
        this.lstGroups3 = data;
        console.log(this.lstGroups3);
      }
    );
    this._freeApiService. getGroups4().subscribe
    (
      data=> {
        this.lstGroups4 = data;
        console.log(this.lstGroups4);
      }
    );
    this._freeApiService. getGroups5().subscribe
    (
      data=> {
        this.lstGroups5 = data;
        console.log(this.lstGroups5);
      }
    );
    
    this.hideTextareaGroups();
    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
    this.hideTextareaGroups4();
    this.hideTextareaGroups5();
  }

  onSubmit(form:NgForm):void{

    this.bundle.isbuylimited = this.lstIsObject["isbuylimited"];
    this.bundle.isBuyableForOther = this.lstIsObject["isBuyableForOther"];
    this.bundle.isBuyableWithLanyCredit = this.lstIsObject["isBuyableWithLanyCredit"];
    this.bundle.isBuyableByOM = this.lstIsObject["isBuyableByOM"];
    this.bundle.isautorenewable = this.lstIsObject["isautorenewable"];

    console.log(form.value);

    //Récupérer la valeur du groups textarea
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(this.bundle.groups=="autre"){
        this.bundle.groups = textarea.value;
      }

    //Récupérer la valeur du groups1 textarea1
    var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
      if(this.bundle.groups1=="autre"){
        this.bundle.groups1 = textarea1.value;
      }

    //Récupérer la valeur du groups2 textarea2
    var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
      if(this.bundle.groups2=="autre"){
        this.bundle.groups2 = textarea2.value;
      }

    //Récupérer la valeur du groups3 textarea3
    var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      if(this.bundle.groups3=="autre"){
        this.bundle.groups3 = textarea3.value;
      }

    //Récupérer la valeur du groups4 textarea4
    var textarea4 = (<HTMLTextAreaElement>document.getElementById("textareaGroups4")); 
      if(this.bundle.groups4=="autre"){
        this.bundle.groups4 = textarea4.value;
      }

    //Récupérer la valeur du groups5 textarea5
    var textarea5 = (<HTMLTextAreaElement>document.getElementById("textareaGroups5")); 
      if(this.bundle.groups5=="autre"){
        this.bundle.groups5 = textarea5.value;
      }

    console.log("this.bundle", this.bundle);

    if(this.bundle.isbuylimited==1){
      if(this.bundle.buyfrequency!="" && this.bundle.buymaximum!=0){
        this._freeApiService.CreateBundle(this.bundle).subscribe(
          resp=>{
            console.log(resp);
            this.bundle.idbundle=resp.idbundle;
    
            console.log('Bundle submit: ',this.bundle);
    
            //################Bundleofferelementaire################
    
              //Get all quantity and add in array listquantity
              var x = document.querySelectorAll("div.quantity");
              x.forEach(e =>{
                for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                  this.listQuantity.push(e.getElementsByTagName("input")[i].value);
                }
              }); 
    
              //Turn listDA and listQuantity to Object table associatif (clé -> value)
              for(let i = 0; i < this.listDA.length; i++){
                //inserting new key value pair inside map
                this.listDAquantity[this.listDA[i]] =  this.listQuantity[i];   
                
              };
    
                console.log(this.listDA);
                console.log(this.listQuantity);
                console.log(this.listDAquantity);
    
                this._freeApiService.CreateBundleofferelementaire(this.bundle.idbundle, this.listDAquantity).subscribe(
                  resp=>{
                    console.log(resp);
    
                    //################Profilebundle################
    
                      this._freeApiService.CreateProfilebundle(this.bundle.idbundle, this.listeSelected).subscribe(
                        res => {
                        console.log('Response: ', res);
                        console.log(res);
                        
    
                          Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: 'La nouvelle offre est créée avec succès',
                            showConfirmButton: false,
                            timer: 2500
                            
                          })
    
                          //Décocher tous les natures offerelementaire
                          var x = document.querySelectorAll("div.mycontainer");
                          x.forEach(e =>{
                            for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                              e.getElementsByTagName("input")[i].checked=false;
                            }
                          }); 
                          
                          this.ngOnInit();
    
                          },
                          error1=>{
                            Swal.fire({
                              position: 'center',
                              icon: 'error',
                              text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                              showConfirmButton: false,
                              timer: 2500
                            })
                          }
                      );
                    
                    }
             
                );
          }
            
          
        );
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Vous venez de cliquer sur "isBuylimited", alors vous devez préciser "Buyfrequency" et "Buymaximum"',
          showConfirmButton: false,
          timer: 5000
        })
      }

    }
    else{
      this._freeApiService.CreateBundle(this.bundle).subscribe(
        resp=>{
          console.log(resp);
          this.bundle.idbundle=resp.idbundle;
  
          console.log('Bundle submit: ',this.bundle);
  
          //################Bundleofferelementaire################
  
            //Get all quantity and add in array listquantity
            var x = document.querySelectorAll("div.quantity");
            x.forEach(e =>{
              for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                this.listQuantity.push(e.getElementsByTagName("input")[i].value);
              }
            }); 
  
            //Turn listDA and listQuantity to Object table associatif (clé -> value)
            for(let i = 0; i < this.listDA.length; i++){
              //inserting new key value pair inside map
              this.listDAquantity[this.listDA[i]] =  this.listQuantity[i];   
              
            };
  
              console.log(this.listDA);
              console.log(this.listQuantity);
              console.log(this.listDAquantity);
  
              this._freeApiService.CreateBundleofferelementaire(this.bundle.idbundle, this.listDAquantity).subscribe(
                resp=>{
                  console.log(resp);
  
                  //################Profilebundle################
  
                    this._freeApiService.CreateProfilebundle(this.bundle.idbundle, this.listeSelected).subscribe(
                      res => {
                      console.log('Response: ', res);
                      console.log(res);
                      
  
                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          text: 'La nouvelle offre est créée avec succès',
                          showConfirmButton: false,
                          timer: 2500
                          
                        })
  
                        //Décocher tous les natures offerelementaire
                        var x = document.querySelectorAll("div.mycontainer");
                        x.forEach(e =>{
                          for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                            e.getElementsByTagName("input")[i].checked=false;
                          }
                        }); 
                        
                        this.ngOnInit();
  
                        },
                        error1=>{
                          Swal.fire({
                            position: 'center',
                            icon: 'error',
                            text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                            showConfirmButton: false,
                            timer: 2500
                          })
                        }
                    );
                  
                  }
           
              );
        }
          
        
      );
    }

  }

  changeValueCheckbox(e: any) {
    if(e.target.checked){
      console.log('Event chang checkbox: ', e, e.target.value, this.listeSelected);
      this.listeSelected.push(e.target.value);
    }
    else{
      this.listeSelected=this.listeSelected.filter(m=>m!=e.target.value);
      console.log(e.target.value + 'UNChecked');
    }
    console.log(this.listeSelected);
  }
  changeValueCheckboxNature(e: any) {
     
    if(e.target.checked){

    this.listeSelectedNature.push(e.target.value);
    this._freeApiService.getDAofAllNatures(this.listeSelectedNature).subscribe(res => {
      this.listDA = res;

    },error => {
      console.log(error);
    });

    this.listUnit=[];
    this._freeApiService.getUnitofAllNatures(this.listeSelectedNature).subscribe(res => {
      for(let i = 0; i < res.length; i++){
        if(res[i]=="1"){
          this.listUnit.push("SMS");
        }
        else if(res[i]=="2"){
          this.listUnit.push("Ariary");
        }
        else if(res[i]=="3"){
          this.listUnit.push("Seconde");
        }
        else if(res[i]=="4"){
          this.listUnit.push("Octet");
        }
        else{
          this.listUnit.push("Unité inconnue");
        }
        
      }
      console.log(this.listUnit);

    },error => {
      console.log(error);
    });

      console.log('Cocher ', this.listeSelectedNature, e.target.value + ' Checked');
      console.log("listDA cocher ",this.listDA );
      console.log(this.listUnit);

  }

    else{

      this.listeSelectedNature=this.listeSelectedNature.filter(m=>m!=e.target.value);
      this._freeApiService.getDAofAllNatures(this.listeSelectedNature).subscribe(res => {
        this.listDA = res;

      },error => {
        console.log(error);
      });

      this.listUnit=[];
      this._freeApiService.getUnitofAllNatures(this.listeSelectedNature).subscribe(res => {
        for(let i = 0; i < res.length; i++){
          if(res[i]=="1"){
            this.listUnit.push("SMS");
          }
          else if(res[i]=="2"){
            this.listUnit.push("Ariary");
          }
          else if(res[i]=="3"){
            this.listUnit.push("Seconde");
          }
          else if(res[i]=="4"){
            this.listUnit.push("Octet");
          }
          else{
            this.listUnit.push("Unité inconnue");
          }
          
        }
        console.log(this.listUnit);
        
  
      },error => {
        console.log(error);
      });

      console.log('Decocher: ', this.listeSelectedNature,e.target.value + ' UNChecked');
      console.log("listDA decocher ",this.listDA );
      console.log(this.listUnit); 
       
    }
    
  }

  changeValueCheckboxIs(e: any) {
    if(e.target.checked){
      this.lstIsObject[e.target.value] = 1;

      if(e.target.value == "isbuylimited"){
        this.isBuyLimitedChecked(true);
      }
    }
    else{
      this.lstIsObject[e.target.value] = 0;
      
      if(e.target.value == "isbuylimited"){
        this.isBuyLimitedChecked(false);
      }
    }
    console.log("this.lstIsObject : ",this.lstIsObject);
  }
  isBuyLimitedChecked(isChecked:boolean){
    var x = document.querySelectorAll('div.buyLimitedChecked');
    if(isChecked==true){
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=false;
        }
      });
    }
    else{
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=true;
        }
      });
      this.bundle.buyfrequency="";
      this.bundle.buymaximum=0;
    }
    

  }


  /*List Groups*/
  changeSelectedGroups(e:any){
    //Saisir un nouveau groups hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups(){
    //Cacher textareaGroups
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups1(e:any){
    //Saisir un nouveau groups1 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups1(){
    //Cacher textareaGroups1
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups2(e:any){
    //Saisir un nouveau groups2 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups2(){
    //Cacher textareaGroups2
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups3(e:any){
    //Saisir un nouveau groups3 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups3(){
    //Cacher textareaGroups3
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups4(e:any){
    //Saisir un nouveau groups4 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups4(){
    //Cacher textareaGroups4
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups5(e:any){
    //Saisir un nouveau groups5 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups5(){
    //Cacher textareaGroups5
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
    textarea.style.display='none';
    textarea.value="";
  }


  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }
 
}