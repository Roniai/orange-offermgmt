import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { BundleConfig} from '../classes/bundleconfig';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-technique-offres-r',
  templateUrl: './technique-offres-r.component.html',
  styleUrls: ['./technique-offres-r.component.css']
})
export class TechniqueOffresRComponent implements OnInit {
  roleuser:any="";

  bundle:BundleConfig= new BundleConfig();
  lstBundleConfig :BundleConfig[];

    /* Profilebundle */
    lstprofile:any[];
    listeSelected: any[] = [];
    listIdprofile:any[] = [];
    mylstprofileObject:any = {};

    /* Checkbox */
    lstIsObject:any = {};

    /*List Groups*/ 
    lstGroups:[]=[];
    lstGroups1:[]=[];
    lstGroups2:[]=[];
    lstGroups3:[]=[];
    lstGroups4:[]=[];
    lstGroups5:[]=[];


  constructor( private _freeApiService: freeApiService,private authService: AuthService) {this.lstBundleConfig=[];this.lstprofile=[];} 
    

  ngOnInit(): void {

    this.roleuser = this.authService.roleuserService;

    this.bundle = new BundleConfig();

    //Checkbox is
    this.lstIsObject["isbuylimited"] = 0;
    this.lstIsObject["isBuyableForOther"] = 0;
    this.lstIsObject["isBuyableWithLanyCredit"] = 0;
    this.lstIsObject["isBuyableByOM"] = 0;
    this.lstIsObject["isautorenewable"] = 0;

    //Technique
    this.lstIsObject["isActive"] = 0;
    this.lstIsObject["isroaming"] = 0;

    ///get
    this._freeApiService. getBundle().subscribe
    (
      data=> {
        this.lstBundleConfig = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );


    /*List Groups*/
    this._freeApiService. getGroups().subscribe
    (
      data=> {
        this.lstGroups = data;
        console.log(this.lstGroups);
      }
    );
    this._freeApiService. getGroups1().subscribe
    (
      data=> {
        this.lstGroups1 = data;
        console.log(this.lstGroups1);
      }
    );
    this._freeApiService. getGroups2().subscribe
    (
      data=> {
        this.lstGroups2 = data;
        console.log(this.lstGroups2);
      }
    );
    this._freeApiService. getGroups3().subscribe
    (
      data=> {
        this.lstGroups3 = data;
        console.log(this.lstGroups3);
      }
    );
    this._freeApiService. getGroups4().subscribe
    (
      data=> {
        this.lstGroups4 = data;
        console.log(this.lstGroups4);
      }
    );
    this._freeApiService. getGroups5().subscribe
    (
      data=> {
        this.lstGroups5 = data;
        console.log(this.lstGroups5);
      }
    );

    this.hideTextareaGroups();
    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
    this.hideTextareaGroups4();
    this.hideTextareaGroups5();
  }

    
  onSubmit(form:NgForm):void{
      console.log(form.value);

      this.bundle.isbuylimited = this.lstIsObject["isbuylimited"];
      this.bundle.isBuyableForOther = this.lstIsObject["isBuyableForOther"];
      this.bundle.isBuyableWithLanyCredit = this.lstIsObject["isBuyableWithLanyCredit"];
      this.bundle.isBuyableByOM = this.lstIsObject["isBuyableByOM"];
      this.bundle.isautorenewable = this.lstIsObject["isautorenewable"];

      //Technique
      this.bundle.isActive = this.lstIsObject["isActive"];
      this.bundle.isroaming = this.lstIsObject["isroaming"];

      //List Groups
      //Récupérer la valeur du groups textarea
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(this.bundle.groups=="autre"){
        this.bundle.groups = textarea.value;
      }

      //Récupérer la valeur du groups1 textarea1
      var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
        if(this.bundle.groups1=="autre"){
          this.bundle.groups1 = textarea1.value;
        }

      //Récupérer la valeur du groups2 textarea2
      var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
        if(this.bundle.groups2=="autre"){
          this.bundle.groups2 = textarea2.value;
        }

      //Récupérer la valeur du groups3 textarea3
      var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(this.bundle.groups3=="autre"){
          this.bundle.groups3 = textarea3.value;
        }

      //Récupérer la valeur du groups4 textarea4
      var textarea4 = (<HTMLTextAreaElement>document.getElementById("textareaGroups4")); 
        if(this.bundle.groups4=="autre"){
          this.bundle.groups4 = textarea4.value;
        }

      //Récupérer la valeur du groups5 textarea5
      var textarea5 = (<HTMLTextAreaElement>document.getElementById("textareaGroups5")); 
        if(this.bundle.groups5=="autre"){
          this.bundle.groups5 = textarea5.value;
        }
      
      console.log(this.bundle);

      //isBuyLimited is checked
      if(this.bundle.isbuylimited==1){
        if(this.bundle.buyfrequency!="" && this.bundle.buymaximum!=0){
          //################Bundle################
          this._freeApiService.UpdateBundleMkTc(this.bundle).subscribe(
            resp=>{
              console.log(resp); 
    
              //################Profilebundle################
                      
                //Delete all profile
                this._freeApiService.DeleteProfilebundleById(this.bundle.idbundle).subscribe(
                  resp=>{
                    console.log(resp);
        
                      //Create new profiles
                      this._freeApiService.CreateProfilebundle(this.bundle.idbundle , this.listeSelected).subscribe(
                        res => {
                      
                          Swal.fire({
                            position: 'center',
                            icon: 'success',
                            text: 'La mise à jour de l\'offre "'+this.bundle.nom+'" a été bien effectuée',
                            showConfirmButton: false,
                            timer: 2500
                          })
                          this.closeModale();
                        
                            },
                          error1=>{
                      
                            Swal.fire({
                              position: 'center',
                              icon: 'error',
                              text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau à nouveau',
                              showConfirmButton: false,
                              timer: 2500
                            })
                      });
                });
          
          });
        }
        else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Vous venez de cliquer sur "isBuylimited", alors vous devez préciser "Buyfrequency" et "Buymaximum"',
            showConfirmButton: false,
            timer: 5000
          })
        }
      }
      else{
        //################Bundle################
        this._freeApiService.UpdateBundleMkTc(this.bundle).subscribe(
          resp=>{
            console.log(resp); 
  
            //################Profilebundle################
                    
              //Delete all profile
              this._freeApiService.DeleteProfilebundleById(this.bundle.idbundle).subscribe(
                resp=>{
                  console.log(resp);
      
                    //Create new profiles
                    this._freeApiService.CreateProfilebundle(this.bundle.idbundle , this.listeSelected).subscribe(
                      res => {
                    
                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          text: 'La mise à jour de l\'offre "'+this.bundle.nom+'" a été bien effectuée',
                          showConfirmButton: false,
                          timer: 2500
                        })
                        this.closeModale();
                      
                          },
                        error1=>{
                    
                          Swal.fire({
                            position: 'center',
                            icon: 'error',
                            text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau à nouveau',
                            showConfirmButton: false,
                            timer: 2500
                          })
                    });
              });
        
        });
      }
  }

    edit(data:BundleConfig):void{
  
      this.bundle=data;

      this.lstIsObject["isbuylimited"] = this.bundle.isbuylimited;
      this.lstIsObject["isBuyableForOther"] = this.bundle.isBuyableForOther;
      this.lstIsObject["isBuyableWithLanyCredit"] = this.bundle.isBuyableWithLanyCredit;
      this.lstIsObject["isBuyableByOM"] = this.bundle.isBuyableByOM;
      this.lstIsObject["isautorenewable"] = this.bundle.isautorenewable;


      //############### Marketing Level ###############

      //isBuyableForOther
      if(this.bundle.isBuyableForOther==1){
        this.isBuyableForOtherChecked(true);
      }
      else{
        this.isBuyableForOtherChecked(false);
      }

      //isautorenewable
      if(this.bundle.isautorenewable==1){
        this.isautorenewableChecked(true);
      }
      else{
       this.isautorenewableChecked(false);
      }

      //isBuyableByOM
      if(this.bundle.isBuyableByOM==1){
        this.isBuyableByOMChecked(true);
      }
      else{
        this.isBuyableByOMChecked(false);
      }

      //isBuyableWithLanyCredit
      if(this.bundle.isBuyableWithLanyCredit==1){
        this.isBuyableWithLanyCreditChecked(true);
      }
      else{
        this.isBuyableWithLanyCreditChecked(false);
      }
      
      //isBuyLimited is checked => enable buyfrequence et buymaximum
      if(this.bundle.isbuylimited==1){
        this.isBuyLimitedChecked(true);
      }
      else{
        this.isBuyLimitedChecked(false);
      }

      //Profilebundle
        this._freeApiService.GetIdprofileofBundle(data.idbundle).subscribe(
          resp=>{
            this.listIdprofile = resp;

            //Initialisation
            this.listeSelected=[];
            this.mylstprofileObject={};

              for (let i = 0; i < this.lstprofile.length ; i++){
                //Si l'élément existe déjà
                if(this.listIdprofile.indexOf(this.lstprofile[i])!=-1){
                    //Tableau associatif pour stocker "checked=true or false"
                    this.mylstprofileObject[this.lstprofile[i]] = true;

                    this.listeSelected.push(this.lstprofile[i]);
                    
                }
                else{
                  this.mylstprofileObject[this.lstprofile[i]] = false;
                }

              }

              //Actualiser lstprofile pour refaire *ngFor="let profile of lstprofile"
              this.lstprofile=[];
              this.refreshListeProfile();

              console.log("this.mylstprofileObject : ",this.mylstprofileObject);
              console.log("this.listeSelected : ",this.listeSelected);
        });




      //############### Technique Level ###############
      this.lstIsObject["isActive"] = this.bundle.isActive;
      this.lstIsObject["isroaming"] = this.bundle.isroaming;

      console.log("this.lstIsObject : ",this.lstIsObject);

    }

    changeValueCheckbox(e: any) {
      if(e.target.checked){
        console.log(e.target.value + 'Checked')
        this.listeSelected.push(e.target.value);
        console.log(this.listeSelected);
      }
      else{
        this.listeSelected=this.listeSelected.filter(m=>m!=e.target.value);
        console.log(e.target.value + 'UNChecked');
        console.log(this.listeSelected);
      }
      
    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;
  
        if(e.target.value == "isbuylimited"){
          this.isBuyLimitedChecked(true);
        }
        else if(e.target.value == "isBuyableForOther"){
          this.isBuyableForOtherChecked(true);
        }
        else if(e.target.value == "isautorenewable"){
          this.isautorenewableChecked(true);
        }
        else if(e.target.value == "isBuyableByOM"){
          this.isBuyableByOMChecked(true);
        }
        else if(e.target.value == "isBuyableWithLanyCredit"){
          this.isBuyableWithLanyCreditChecked(true);
        }
      }
      else{
        this.lstIsObject[e.target.value] = 0;
        
        if(e.target.value == "isbuylimited"){
          this.isBuyLimitedChecked(false);
        }
        else if(e.target.value == "isBuyableForOther"){
          this.isBuyableForOtherChecked(false);
        }
        else if(e.target.value == "isautorenewable"){
          this.isautorenewableChecked(false);
        }
        else if(e.target.value == "isBuyableByOM"){
          this.isBuyableByOMChecked(false);
        }
        else if(e.target.value == "isBuyableWithLanyCredit"){
          this.isBuyableWithLanyCreditChecked(false);
        }
      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }

    isBuyLimitedChecked(isChecked:boolean){
      var x = document.querySelectorAll('div.buyLimitedChecked');
      if(isChecked==true){
        x.forEach(e=>{
          for(let i=0; i<e.getElementsByTagName('input').length; i++){
            e.getElementsByTagName('input')[i].disabled=false;
          }
        });
      }
      else{
        x.forEach(e=>{
          for(let i=0; i<e.getElementsByTagName('input').length; i++){
            e.getElementsByTagName('input')[i].disabled=true;
          }
        });
        this.bundle.buyfrequency="";
        this.bundle.buymaximum=0;
      }
    }
    isBuyableForOtherChecked(isChecked:boolean){
      var input = (<HTMLInputElement>document.getElementById("refillfwd"));
      if(isChecked==true){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refillfwd="";
      }
    }
    isautorenewableChecked(isChecked:boolean){
      var x = (<HTMLDivElement>document.getElementById('Pam'));
      if(isChecked==true){
        for(let i=0; i<x.getElementsByTagName('input').length; i++){
          x.getElementsByTagName('input')[i].disabled=false;
        }
      }
      else{
        for(let i=0; i<x.getElementsByTagName('input').length; i++){
          x.getElementsByTagName('input')[i].disabled=true;
        }
        this.bundle.pamclass=0;
        this.bundle.pamscheduled=0;     
        this.bundle.pamservice=0; 
      }
    }
    isBuyableByOMChecked(isChecked:boolean){
      var input = (<HTMLInputElement>document.getElementById("refillom"));
      if(isChecked==true){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refillom="";
      }
    }
    isBuyableWithLanyCreditChecked(isChecked:boolean){
      var input = (<HTMLInputElement>document.getElementById("refilllanycredit"));
      if(isChecked==true){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refilllanycredit="";
      }
    }

    /*List Groups*/
    changeSelectedGroups(e:any){
      //Saisir un nouveau groups hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups(){
      //Cacher textareaGroups
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups1(e:any){
      //Saisir un nouveau groups1 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups1(){
      //Cacher textareaGroups1
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups2(e:any){
      //Saisir un nouveau groups2 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups2(){
      //Cacher textareaGroups2
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups3(e:any){
      //Saisir un nouveau groups3 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups3(){
      //Cacher textareaGroups3
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups4(e:any){
      //Saisir un nouveau groups4 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups4(){
      //Cacher textareaGroups4
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups5(e:any){
      //Saisir un nouveau groups5 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups5(){
      //Cacher textareaGroups5
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
      textarea.style.display='none';
      textarea.value="";
    }
 
    
    refreshListeProfile(){
      this._freeApiService. getProfileIdprofile().subscribe
      (
        data=> {
          this.lstprofile = data;
        }
      );
    }

    onlyInteger(e:any){
      return e.charCode >= 48 && e.charCode <= 57;
    }
    
    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }

}
