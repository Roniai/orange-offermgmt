import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueOffresRComponent } from './technique-offres-r.component';

describe('TechniqueOffresRComponent', () => {
  let component: TechniqueOffresRComponent;
  let fixture: ComponentFixture<TechniqueOffresRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueOffresRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueOffresRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
