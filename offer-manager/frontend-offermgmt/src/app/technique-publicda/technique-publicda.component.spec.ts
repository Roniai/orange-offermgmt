import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniquePublicdaComponent } from './technique-publicda.component';

describe('TechniquePublicdaComponent', () => {
  let component: TechniquePublicdaComponent;
  let fixture: ComponentFixture<TechniquePublicdaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniquePublicdaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniquePublicdaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
