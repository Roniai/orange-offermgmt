import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { Publicda } from '../classes/publicda';
import { Offerelementaire} from '../classes/offerelementaire';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-technique-publicda',
  templateUrl: './technique-publicda.component.html',
  styleUrls: ['./technique-publicda.component.css']
})
export class TechniquePublicdaComponent implements OnInit {
  roleuser:any="";
  
  lstPublicda :Publicda[];
  publicda:Publicda= new Publicda();

  isEdit:boolean=false;
  lstprofile:any[];

  lstOfferelementaire :Offerelementaire[];

  /* Checkbox */
  lstIsObject:any = {};

  /*List Groups*/ 
  lstGroups1:[]=[];
  lstGroups2:[]=[];
  lstGroups3:[]=[];

  constructor( private _freeApiService: freeApiService,private authService: AuthService) { this.lstPublicda=[];this.lstprofile=[]; this.lstOfferelementaire=[]; }

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;
    
    this.lstIsObject["isroaming"] = 0;
    this.lstIsObject["istoexcludefrominfoconso"] = 0;
    this.lstIsObject["ismixed"] = 0;
    this.lstIsObject["isActive"] = 0;
    
    ///get
    this._freeApiService. getPublicda().subscribe
    (
      data=> {
        this.lstPublicda = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );
    this._freeApiService.getOfferelementaire().subscribe
    (
      data=> {
        this.lstOfferelementaire = data;
      }
    );

    /*List Groups*/
    this._freeApiService. getGroups1Publicda().subscribe
    (
      data=> {
        this.lstGroups1 = data;
        console.log(this.lstGroups1);
      }
    );
    this._freeApiService. getGroups2Publicda().subscribe
    (
      data=> {
        this.lstGroups2 = data;
        console.log(this.lstGroups2);
      }
    );
    this._freeApiService. getGroups3Publicda().subscribe
    (
      data=> {
        this.lstGroups3 = data;
        console.log(this.lstGroups3);
      }
    );

    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
  }
    
    onSubmit(form:NgForm):void{

      //List Groups
      //Récupérer la valeur du groups1 textarea1
      var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
        if(this.publicda.groups1=="autre"){
          this.publicda.groups1 = textarea1.value;
        }

      //Récupérer la valeur du groups2 textarea2
      var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
        if(this.publicda.groups2=="autre"){
          this.publicda.groups2 = textarea2.value;
        }

      //Récupérer la valeur du groups3 textarea3
      var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(this.publicda.groups3=="autre"){
          this.publicda.groups3 = textarea3.value;
        }

      if(!this.isEdit){
        console.log(form.value);

        this.publicda.isroaming = this.lstIsObject["isroaming"];
        this.publicda.istoexcludefrominfoconso = this.lstIsObject["istoexcludefrominfoconso"];
        this.publicda.ismixed = this.lstIsObject["ismixed"];
        this.publicda.isActive = this.lstIsObject["isActive"];

        if(this.publicda.ismixed==1){
          var x = document.querySelectorAll('div.Alltarifs');
          let difZero = 0;
          x.forEach(e=>{
            for(let i=0; i<e.getElementsByTagName('input').length; i++){
              if(+e.getElementsByTagName('input')[i].value != 0){
                difZero++;
              }
            }
          });

          if(difZero<2){
            Swal.fire({
              position: 'center',
              icon: 'error',
              text: 'Vous venez de cliquer sur "isMixed", alors vous devez compléter deux champs minimum parmi ces neuf tarifs',
              showConfirmButton: false,
              timer: 4000
            })
          }
          else{ 
            console.log("this.publicda = ",this.publicda);

            this._freeApiService.CreatePublicda(this.publicda).subscribe(
              resp=>{
                console.log(resp);
                console.log("swall");
  
                this.closeModale();

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  text: 'La nouvelle publicda est créée avec succès',
                  showConfirmButton: false,
                  timer: 2500
                })
          
              },
              error1=>{
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                  showConfirmButton: false,
                  timer: 2500
                })
            });
          }
          console.log(difZero); 
        }
        else{
          console.log("this.publicda = ",this.publicda);

          this._freeApiService.CreatePublicda(this.publicda).subscribe(
            resp=>{
              console.log(resp);
              console.log("swall");

              this.closeModale();

              Swal.fire({
                position: 'center',
                icon: 'success',
                text: 'La nouvelle publicda est créée avec succès',
                showConfirmButton: false,
                timer: 2500
              })
        
            },
            error1=>{
              Swal.fire({
                position: 'center',
                icon: 'error',
                text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                showConfirmButton: false,
                timer: 2500
              })
          });
        }

      }
      //PUt
      else{
      console.log(form.value);

      this.publicda.isroaming = this.lstIsObject["isroaming"];
      this.publicda.istoexcludefrominfoconso = this.lstIsObject["istoexcludefrominfoconso"];
      this.publicda.ismixed = this.lstIsObject["ismixed"];
      this.publicda.isActive = this.lstIsObject["isActive"];

          if(this.publicda.ismixed==1){
            var x = document.querySelectorAll('div.Alltarifs');
            let difZero = 0;
            x.forEach(e=>{
              for(let i=0; i<e.getElementsByTagName('input').length; i++){
                if(+e.getElementsByTagName('input')[i].value != 0){
                  difZero++;
                }
              }
            });
            if(difZero<2){
              Swal.fire({
                position: 'center',
                icon: 'error',
                text: 'Vous venez de cliquer sur "isMixed", alors vous devez compléter au moins deux champs parmi ces neuf tarifs',
                showConfirmButton: false,
                timer: 4000
              })
            }
            else{ 
              console.log("this.publicda = ",this.publicda);

              this._freeApiService.UpdatePublicda(this.publicda).subscribe(
                resp=>{
                  console.log(resp);
                  console.log("swall");
    
                  this.closeModale();

                  Swal.fire({
                    position: 'center',
                    icon: 'success',
                    text: 'La mise à jour de la publicda "'+this.publicda.nom+'" a été bien effectuée',
                    showConfirmButton: false,
                    timer: 2500
                  })
                
                    },
                  error1=>{
              
                    Swal.fire({
                      position: 'center',
                      icon: 'error',
                      text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                      showConfirmButton: false,
                      timer: 2500
                    })
              });
            }
            console.log(difZero); 
          }
          else{
            console.log("this.publicda = ",this.publicda);

            this._freeApiService.UpdatePublicda(this.publicda).subscribe(
              resp=>{
                console.log(resp);
                console.log("swall");

                this.closeModale();

                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  text: 'La mise à jour de la publicda "'+this.publicda.nom+'" a été bien effectuée',
                  showConfirmButton: false,
                  timer: 2500
                })
              
                  },
                error1=>{
            
                  Swal.fire({
                    position: 'center',
                    icon: 'error',
                    text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                    showConfirmButton: false,
                    timer: 2500
                  })
            });
          }

      }
    
    }
    
    add():void{
      this.isEdit=false;
      this.publicda=new Publicda;

      this.lstIsObject["isroaming"] = 0;
      this.lstIsObject["istoexcludefrominfoconso"] = 0;
      this.lstIsObject["ismixed"] = 0;
      this.lstIsObject["isActive"] = 0;

      this.disabledAllTarifs();

      this.hideTextareaGroups1();
      this.hideTextareaGroups2();
      this.hideTextareaGroups3();
      
    }

    //Put(maka value fotsiny )
    edit(data:Publicda):void{
      this.isEdit=true;
      this.publicda=data;

      this.lstIsObject["isroaming"] = this.publicda.isroaming;
      this.lstIsObject["istoexcludefrominfoconso"] = this.publicda.istoexcludefrominfoconso;
      this.lstIsObject["ismixed"] = this.publicda.ismixed;
      this.lstIsObject["isActive"] = this.publicda.isActive;

      if(this.publicda.ismixed==0){
        this.disabledAllTarifs();
      }
      else{
        this.enabledAllTarifs();
      }
    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;

        if(e.target.value == "ismixed"){
          this.enabledAllTarifs();
        }

      }
      else{
        this.lstIsObject[e.target.value] = 0;

        if(e.target.value == "ismixed"){
          this.disabledAllTarifs();
        }

      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }

    disabledAllTarifs(){
      //Mettre tous les tarifs disabled
      var x = document.querySelectorAll('div.Alltarifs');
      let difZero = 0;
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=true;
        }
      });

      this.publicda.tarifappelfamille=0;
      this.publicda.tarifappelinternational=0;
      this.publicda.tarifappelnat=0;
      this.publicda.tarifappelorange=0;
      this.publicda.tarifdata=0;
      this.publicda.tarifsmsfamille=0;
      this.publicda.tarifsmsinternational=0;
      this.publicda.tarifsmsnat=0;
      this.publicda.tarifsmsorange=0;

    }
    enabledAllTarifs(){
      //Mettre tous les tarifs enabled
      var x = document.querySelectorAll('div.Alltarifs');
      let difZero = 0;
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=false;
        }
      });
    }

    /*List Groups*/
    changeSelectedGroups1(e:any){
      //Saisir un nouveau groups1 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups1(){
      //Cacher textareaGroups1
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups2(e:any){
      //Saisir un nouveau groups2 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups2(){
      //Cacher textareaGroups2
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups3(e:any){
      //Saisir un nouveau groups3 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups3(){
      //Cacher textareaGroups3
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      textarea.style.display='none';
      textarea.value="";
    }

    onlyInteger(e:any){
      return e.charCode >= 48 && e.charCode <= 57;
    }
    
    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }


    
    //delete
    /* delete(data:Publicda):void{
      window.confirm("vous etes sur")
      this._freeApiService.Delete(data.idprofile || data.DA).subscribe(
        resp=>{
          this.ngOnInit();
        }
      )
    } */


  }
