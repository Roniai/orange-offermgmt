import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Otherservice } from '../classes/otherservice';
import { Profileotherservice } from '../classes/profileotherservice';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-otherservice',
  templateUrl: './admin-otherservice.component.html',
  styleUrls: ['./admin-otherservice.component.css']
})
export class AdminOtherserviceComponent implements OnInit {


  lstOtherservice :Otherservice[];
  otherservice:Otherservice;
  isEdit:boolean=false;

  /* Profileotherservice */
  lstprofile:any[];
  listeSelected: any[] = [];
  listIdprofile:any[] = [];
  mylstprofileObject:any = {};

  /* Checkbox */
  lstIsObject:any = {};

  /*List Groups*/ 
  lstGroups:[]=[];
  lstGroups1:[]=[];
  lstGroups2:[]=[];
  lstGroups3:[]=[];

  constructor( private _freeApiService: freeApiService) { this.otherservice= new Otherservice();this.lstOtherservice=[]; this.lstprofile=[];}

  ngOnInit(): void {
    this.otherservice= new Otherservice();
    this.listeSelected=[];
    this.isEdit=false;

    this.lstIsObject["isActive"] = 0;

    //Taxe = TTC par défaut
    this.otherservice.taxe="TTC";

    ///get
    this._freeApiService. getOtherservice().subscribe
    (
      data=> {
        this.lstOtherservice = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );

        /*List Groups*/
        this._freeApiService. getGroupsOtherservice().subscribe
        (
          data=> {
            this.lstGroups = data;
            console.log(this.lstGroups);
          }
        );
        this._freeApiService. getGroups1Otherservice().subscribe
        (
          data=> {
            this.lstGroups1 = data;
            console.log(this.lstGroups1);
          }
        );
        this._freeApiService. getGroups2Otherservice().subscribe
        (
          data=> {
            this.lstGroups2 = data;
            console.log(this.lstGroups2);
          }
        );
        this._freeApiService. getGroups3Otherservice().subscribe
        (
          data=> {
            this.lstGroups3 = data;
            console.log(this.lstGroups3);
          }
        );
    
        this.hideTextareaGroups();
        this.hideTextareaGroups1();
        this.hideTextareaGroups2();
        this.hideTextareaGroups3();
    }
    //--------POST-----------/
    
  onSubmit(form:NgForm):void{
      //List Groups
      //Récupérer la valeur du groups textarea
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(this.otherservice.groups=="autre"){
        this.otherservice.groups = textarea.value;
      }

      //Récupérer la valeur du groups1 textarea1
      var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
        if(this.otherservice.groups1=="autre"){
          this.otherservice.groups1 = textarea1.value;
        }

      //Récupérer la valeur du groups2 textarea2
      var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
        if(this.otherservice.groups2=="autre"){
          this.otherservice.groups2 = textarea2.value;
        }

      //Récupérer la valeur du groups3 textarea3
      var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(this.otherservice.groups3=="autre"){
          this.otherservice.groups3 = textarea3.value;
        }

    if(!this.isEdit){
    console.log(form.value);
    this.otherservice.isActive = this.lstIsObject["isActive"];

      this._freeApiService.CreateOtherservice(this.otherservice).subscribe(
      resp=>{
        console.log(resp);
        this.otherservice.idotherservice=resp.idotherservice;

        //################Profileotherservice################
        this._freeApiService.CreateProfileotherservice(this.otherservice.idotherservice , this.listeSelected).subscribe(res => {
        
          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'Le nouveau service est créé avec succès',
            showConfirmButton: false,
            timer: 2500
          })

          this.closeModale();    
      
          },
          error1=>{
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                showConfirmButton: false,
                timer: 2500
              })
          }
        );

      });

    }
    //PUt
      else{
           console.log(form.value);
           this.otherservice.isActive = this.lstIsObject["isActive"];

          this._freeApiService.UpdateOtherservice(this.otherservice).subscribe(
            resp=>{
              console.log(resp);

              //################Profileotherservice################
              //Delete all profile
              this._freeApiService.DeleteProfileotherserviceById(this.otherservice.idotherservice).subscribe(
                resp=>{
                  console.log(resp);

                    //Create new profiles
                    this._freeApiService.CreateProfileotherservice(this.otherservice.idotherservice , this.listeSelected).subscribe(res => {
                    
                      //Décocher tous les profiles cocher avant
                        var x = document.querySelectorAll("div.inputGroup");
                        x.forEach(e =>{
                          for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                            e.getElementsByTagName("input")[i].checked=false;
                          }
                        }); 

                      Swal.fire({
                        position: 'center',
                        icon: 'success',
                        text: 'La mise à jour du service "'+this.otherservice.nom+'" a été bien effectuée',
                        showConfirmButton: false,
                        timer: 2500
                      })
                      
                      this.closeModale();
                  
                      },
                      error1=>{
                          Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                            showConfirmButton: false,
                            timer: 2500
                          })
                      }
                    );
                }
              );

              

            });
      
      }
    }

    add():void{
      this.isEdit=false;
      this.otherservice=new Otherservice;
      this.mylstprofileObject={};
      this.listeSelected=[];
      this.listIdprofile =[];
      this.lstIsObject["isActive"] = 0;

      //Taxe = TTC par défaut
      this.otherservice.taxe="TTC";

      for (let i = 0; i < this.lstprofile.length ; i++){
        //Si l'élément existe déjà
          this.mylstprofileObject[this.lstprofile[i]] = false;  

      }

      //Actualiser lstprofile pour refaire *ngFor="let profile of lstprofile"
      this.lstprofile=[];
      this.refreshListeProfile();

      console.log("this.mylstprofileObject : ",this.mylstprofileObject);
    }

    //Put(maka value fotsiny )
    edit(data:Otherservice):void{
  
      this.isEdit=true;
      this.otherservice=data;
      this.lstIsObject["isActive"] = this.otherservice.isActive;
      
      this._freeApiService.GetIdprofileofOtherservice(data.idotherservice).subscribe(
        resp=>{
          this.listIdprofile = resp;

          //Initialisation
          this.listeSelected=[];
          this.mylstprofileObject={};

          for (let i = 0; i < this.lstprofile.length ; i++){
            //Si l'élément existe déjà
            if(this.listIdprofile.indexOf(this.lstprofile[i])!=-1){
                //Tableau associatif pour stocker "checked=true or false"
                this.mylstprofileObject[this.lstprofile[i]] = true;

                this.listeSelected.push(this.lstprofile[i]);

            }
            else{
              this.mylstprofileObject[this.lstprofile[i]] = false;
            }
    
          }

          //Actualiser lstprofile pour refaire *ngFor="let profile of lstprofile"
          this.lstprofile=[];
          this.refreshListeProfile();

          console.log("this.mylstprofileObject : ",this.mylstprofileObject);

        });
 
    }


    changeValueCheckbox(e: any) {
      if(e.target.checked){
        console.log(e.target.value + 'Checked')
        this.listeSelected.push(e.target.value);
        console.log(this.listeSelected);
      }
      else{
        this.listeSelected=this.listeSelected.filter(m=>m!=e.target.value);
        console.log(e.target.value + 'UNChecked');
        console.log(this.listeSelected);
      }
      
    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;
      }
      else{
        this.lstIsObject[e.target.value] = 0;
      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }

    refreshListeProfile(){
      this._freeApiService. getProfileIdprofile().subscribe
      (
        data=> {
          this.lstprofile = data;
        }
      );
    }

    /*List Groups*/
  changeSelectedGroups(e:any){
    //Saisir un nouveau groups hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups(){
    //Cacher textareaGroups
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups1(e:any){
    //Saisir un nouveau groups1 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups1(){
    //Cacher textareaGroups1
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups2(e:any){
    //Saisir un nouveau groups2 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups2(){
    //Cacher textareaGroups2
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups3(e:any){
    //Saisir un nouveau groups3 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups3(){
    //Cacher textareaGroups3
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
    textarea.style.display='none';
    textarea.value="";
  }


  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }


    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }
  
  }
