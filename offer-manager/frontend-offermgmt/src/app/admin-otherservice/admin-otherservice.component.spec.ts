import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOtherserviceComponent } from './admin-otherservice.component';

describe('AdminOtherserviceComponent', () => {
  let component: AdminOtherserviceComponent;
  let fixture: ComponentFixture<AdminOtherserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminOtherserviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOtherserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
