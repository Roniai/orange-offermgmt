import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogComponent } from './log/log.component';
import { LogCreateComponent } from './log-create/log-create.component';
import { LogUpdateComponent } from './log-update/log-update.component';

import { MarketingOffresComponent } from './marketing-offres/marketing-offres.component';
import { MarketingCreateComponent } from './marketing-create/marketing-create.component';
import { AdminOtherserviceComponent } from './admin-otherservice/admin-otherservice.component';
import { MarketingOffresRComponent } from './marketing-offres-r/marketing-offres-r.component';
import { TechniqueOffresComponent } from './technique-offres/technique-offres.component';
import { TechniqueOffresRComponent } from './technique-offres-r/technique-offres-r.component';
import { TechniqueUnitComponent } from './technique-unit/technique-unit.component';
import { AuthGuard } from './auth.guard';
import { TechniqueProfileComponent } from './technique-profile/technique-profile.component';
import { TechniqueMonthlyComponent } from './technique-monthly/technique-monthly.component';
import { TechniquePublicdaComponent } from './technique-publicda/technique-publicda.component';
import { TechniqueOfferelementaireComponent } from './technique-offerelementaire/technique-offerelementaire.component';
import { TechniqueBundleofferelementaireComponent } from './technique-bundleofferelementaire/technique-bundleofferelementaire.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { EnteteComponent } from './entete/entete.component';


const routes: Routes = [
  {path: 'Marketing-offres', component: MarketingOffresComponent,canActivate:[AuthGuard]},
  {path: 'Marketing-offres-responsable', component: MarketingOffresRComponent,canActivate:[AuthGuard]},
  {path: 'Marketing-create', component: MarketingCreateComponent,canActivate:[AuthGuard]},
  {path: 'Technique-offres', component: TechniqueOffresComponent,canActivate:[AuthGuard]},
  {path: 'Technique-offres-responsable', component: TechniqueOffresRComponent,canActivate:[AuthGuard]},
  {path: 'Technique-unit', component: TechniqueUnitComponent,canActivate:[AuthGuard]},
  {path: 'Technique-profile', component: TechniqueProfileComponent,canActivate:[AuthGuard]},
  {path: 'Technique-monthly', component: TechniqueMonthlyComponent,canActivate:[AuthGuard]},
  {path: 'Technique-publicda', component: TechniquePublicdaComponent,canActivate:[AuthGuard]},
  {path: 'Technique-offerelementaire', component: TechniqueOfferelementaireComponent,canActivate:[AuthGuard]},
  {path: 'Technique-bundleofferelementaire', component: TechniqueBundleofferelementaireComponent,canActivate:[AuthGuard]},
  {path: 'Admin-login', component:AdminLoginComponent,canActivate:[AuthGuard]},
  {path: 'Admin-otherservice', component: AdminOtherserviceComponent,canActivate:[AuthGuard]},
  {path: 'Log-create', component: LogCreateComponent},
  {path: 'Log-update', component: LogUpdateComponent,canActivate:[AuthGuard]},
  {path: 'Entete', component: EnteteComponent,canActivate:[AuthGuard]},
  
  {path: '', component: LogComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
