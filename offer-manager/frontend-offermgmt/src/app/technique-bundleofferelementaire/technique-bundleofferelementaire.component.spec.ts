import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueBundleofferelementaireComponent } from './technique-bundleofferelementaire.component';

describe('TechniqueBundleofferelementaireComponent', () => {
  let component: TechniqueBundleofferelementaireComponent;
  let fixture: ComponentFixture<TechniqueBundleofferelementaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueBundleofferelementaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueBundleofferelementaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
