import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { BundleOfferelementaire} from '../classes/bundleofferelementaire';
import { BundleConfig} from '../classes/bundleconfig';
import Swal from 'sweetalert2';
import { Offerelementaire} from '../classes/offerelementaire';

@Component({
  selector: 'app-technique-bundleofferelementaire',
  templateUrl: './technique-bundleofferelementaire.component.html',
  styleUrls: ['./technique-bundleofferelementaire.component.css']
})
export class TechniqueBundleofferelementaireComponent implements OnInit {
  roleuser:any="";

  lstBundleOfferelementaire :BundleOfferelementaire[];
  data: any = {};
  DA_Before: any = 0;
  offerId_Before: any = 0;

  lstOfferelementaire :Offerelementaire[];
  lstBundleConfig :BundleConfig[];
  lstNom:any={};
  lstNature:any={};

  /* Checkbox */
  lstIsObject:any = {};

  constructor( private _freeApiService: freeApiService,private authService: AuthService) { this.lstBundleOfferelementaire=[]; this.lstOfferelementaire=[]; this.lstBundleConfig=[];}

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;

    this.lstIsObject["ismixed"] = 0;

    ///get
    this._freeApiService. getBundleofferelementaire().subscribe
    (
      data=> {
        this.lstBundleOfferelementaire = data;
      }
    );
    this._freeApiService. getBundle().subscribe
    (
      data=> {
        this.lstBundleConfig = data;
        for(let i=0; i<this.lstBundleConfig.length; i++){
          this.lstNom[this.lstBundleConfig[i].idbundle] = this.lstBundleConfig[i].nom;
        }
      }
    );
    this._freeApiService.getOfferelementaire().subscribe
    (
      data=> {
        this.lstOfferelementaire = data;
        for(let i=0; i<this.lstOfferelementaire.length; i++){
          this.lstNature[this.lstOfferelementaire[i].DA] = this.lstOfferelementaire[i].nature;
        }
      }
    );
    
  }
    //--------POST-----------/
    bundleofferelementaire:BundleOfferelementaire= new BundleOfferelementaire();
    
   onSubmit(form:NgForm):void{
        console.log(form.value);

        this.bundleofferelementaire.ismixed = this.lstIsObject["ismixed"];

        if(this.bundleofferelementaire.ismixed==1){
          var x = document.querySelectorAll('div.Alltarifs');
          let difZero = 0;
          x.forEach(e=>{
            for(let i=0; i<e.getElementsByTagName('input').length; i++){
              if(+e.getElementsByTagName('input')[i].value != 0){
                difZero++;
              }
            }
          });
          if(difZero<2){
            Swal.fire({
              position: 'center',
              icon: 'error',
              text: 'Vous venez de cliquer sur "isMixed", alors vous devez compléter deux champs minimum parmi ces dix tarifs',
              showConfirmButton: false,
              timer: 4000
            })
          }
          else{ 
            console.log("this.bundleofferelementaire = ",this.bundleofferelementaire);

            this._freeApiService.UpdateBundleofferelementaire(this.DA_Before, this.offerId_Before, this.bundleofferelementaire).subscribe(
              resp=>{
                console.log(resp);
          
                this.closeModale();
 
                Swal.fire({
                  position: 'center',
                  icon: 'success',
                  text: 'La mise à jour de la bundleofferelementaire idbundle='+this.bundleofferelementaire.idbundle+', DA='+this.DA_Before+' a été bien effectuée',
                  showConfirmButton: false,
                  timer: 3500
                })
              
                  },
                error1=>{
            
                  Swal.fire({
                    position: 'center',
                    icon: 'error',
                    text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                    showConfirmButton: false,
                    timer: 2500
                  })
            });
          }
          console.log(difZero); 
        }
        else{
          console.log("this.bundleofferelementaire = ",this.bundleofferelementaire);

          this._freeApiService.UpdateBundleofferelementaire(this.DA_Before, this.offerId_Before, this.bundleofferelementaire).subscribe(
            resp=>{
              console.log(resp);
        
              this.closeModale();

              form.resetForm(); 
              Swal.fire({
                position: 'center',
                icon: 'success',
                text: 'La mise à jour de la bundleofferelementaire idbundle='+this.bundleofferelementaire.idbundle+', DA='+this.DA_Before+' a été bien effectuée',
                showConfirmButton: false,
                timer: 2500
              })
            
                },
              error1=>{
          
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                  showConfirmButton: false,
                  timer: 2500
                })
          });
        }

    }
    
    
    //Put(maka value fotsiny )
    edit(data:BundleOfferelementaire):void{

      this.bundleofferelementaire=data;
      this.DA_Before=data.DA;
      this.offerId_Before=data.offerId;
      this.lstIsObject["ismixed"] = this.bundleofferelementaire.ismixed;

      if(this.bundleofferelementaire.ismixed==0){
        this.disabledAllTarifs();
      }
      else{
        this.enabledAllTarifs();
      }
  
    }

    //delete
    delete(data:BundleOfferelementaire):void{
    
      Swal.fire({
        title: 'Êtes-vous sûr?',
        text: "Vous ne pourrez plus revenir en arrière!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        cancelButtonColor: 'black',
        confirmButtonText: 'Oui, supprime-le!',
        confirmButtonColor: '#ff6600' 
  
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'Le champ a été supprimé définitivement',
            showConfirmButton: false,
            timer: 2500
           
          })
          this._freeApiService.DeleteBundleofferelementaire(data).subscribe(
            resp=>{
              
              console.log(resp);
              this.closeModale();
            }
            )
          }
        })
    
    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;

        this.enabledAllTarifs();

      }
      else{
        this.lstIsObject[e.target.value] = 0;

        this.disabledAllTarifs();

      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }

    disabledAllTarifs(){
      //Mettre tous les tarifs disabled
      var x = document.querySelectorAll('div.Alltarifs');
      let difZero = 0;
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=true;
        }
      });

      this.bundleofferelementaire.tarifappelfamille=0;
      this.bundleofferelementaire.tarifappelinternational=0;
      this.bundleofferelementaire.tarifappelnat=0;
      this.bundleofferelementaire.tarifappelorange=0;
      this.bundleofferelementaire.tarifdata=0;
      this.bundleofferelementaire.tarifdatafb=0;
      this.bundleofferelementaire.tarifsmsfamille=0;
      this.bundleofferelementaire.tarifsmsinternational=0;
      this.bundleofferelementaire.tarifsmsnat=0;
      this.bundleofferelementaire.tarifsmsorange=0;

    }
    enabledAllTarifs(){
      //Mettre tous les tarifs enabled
      var x = document.querySelectorAll('div.Alltarifs');
      let difZero = 0;
      x.forEach(e=>{
        for(let i=0; i<e.getElementsByTagName('input').length; i++){
          e.getElementsByTagName('input')[i].disabled=false;
        }
      });
    }

    onlyInteger(e:any){
      return e.charCode >= 48 && e.charCode <= 57;
    }

    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }
    
}
    