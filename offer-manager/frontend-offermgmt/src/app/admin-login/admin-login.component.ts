import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import {Login} from '../classes/login';


@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  lstlogin :Login[];
  data: any = {};
  isEdit:boolean=false;

  /* Checkbox */
  lstIsObject:any = {};
 

  constructor( private _freeApiService: freeApiService, private router: Router ) { this.lstlogin=[]; }

  ngOnInit(): void {
    this.lstIsObject["activeuser"] = 0;

    ///get
    this._freeApiService.getLogin().subscribe
    (
      data=> {
        this.lstlogin = data;
      }
    );
  }
  //--------POST-----------/
  login:Login= new Login();
  
 onSubmit(form:NgForm):void{

  this.login.activeuser = this.lstIsObject["activeuser"];

  if(!this.isEdit){
  this._freeApiService.CreateLogin(this.login).subscribe(
    resp=>{
      console.log(resp);
      
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Le nouveau compte est créé avec succès',
        showConfirmButton: false,
        timer: 2500
      })
      this.closeModale();
   
       },
     error1=>{
      Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
        showConfirmButton: false,
        timer: 2500
      })
    }
   
    
    
   );
   
  }
  //PUt
     else{
       console.log(form.value)
      this._freeApiService.UpdateLogin(this.login).subscribe(
        resp=>{
          console.log(resp);

          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'La mise à jour concernant l\'utilisateur nommée sous le trigramme "'+this.login.trigramme+'" a été bien effectuée',
            showConfirmButton: false,
            timer: 3500
          })
           this.closeModale();
        
            },
          error1=>{
      
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
              showConfirmButton: false,
              timer: 2500
            })
          }
        
      )}

  
  }

  

  add():void{
    this.isEdit=false;
    this.login=new Login;
    this.lstIsObject["activeuser"] = 0;
  }

  //Put(maka value fotsiny )
  edit(data:Login):void{

    this.isEdit=true;
    this.login=data;
    this.lstIsObject["activeuser"] = this.login.activeuser;
    
  }

  //delete
  delete(data:Login):void{
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Annuler',
      cancelButtonColor: 'black',
      confirmButtonText: 'Oui, supprime-le!',
      confirmButtonColor: '#ff6600' 

    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'L\'utilisateur "'+data.trigramme+'" a été supprimé définitivement',
          showConfirmButton: false,
          timer: 2500
         
        })
    
this._freeApiService.DeleteLogin(data.trigramme).subscribe(
  resp=>{
    this.ngOnInit();
    
  
  }
  )
}
})

  }

  changeValueCheckboxIs(e: any) {
    if(e.target.checked){
      this.lstIsObject[e.target.value] = 1;
    }
    else{
      this.lstIsObject[e.target.value] = 0;
    }
    console.log("this.lstIsObject : ",this.lstIsObject);
  }

  closeModale(){
    //Fermer modale
    let x = document.getElementById('id01');
    if(x!=null){x.style.display='none'}
    this.ngOnInit();
  }


}

