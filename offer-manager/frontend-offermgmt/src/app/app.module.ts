import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import{FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule} from '@angular/common/http';
import { freeApiService } from './services/freeapi.service';
import { AuthService } from './services/auth.service'; 
import { AuthGuard } from './auth.guard';
import { EnteteComponent } from './entete/entete.component';

import { LogComponent } from './log/log.component';
import { LogCreateComponent } from './log-create/log-create.component';
import { LogUpdateComponent } from './log-update/log-update.component';

import { NavTechniqueComponent } from './nav-technique/nav-technique.component';
import { NavMarketingComponent } from './nav-marketing/nav-marketing.component';
import { NavAdminComponent } from './nav-admin/nav-admin.component';

import { MarketingOffresComponent } from './marketing-offres/marketing-offres.component';
import { MarketingOffresRComponent } from './marketing-offres-r/marketing-offres-r.component';
import { MarketingCreateComponent } from './marketing-create/marketing-create.component';
import { AdminOtherserviceComponent } from './admin-otherservice/admin-otherservice.component';

import { TechniqueOffresComponent } from './technique-offres/technique-offres.component';
import { TechniqueOffresRComponent } from './technique-offres-r/technique-offres-r.component';
import { TechniqueUnitComponent } from './technique-unit/technique-unit.component';
import { TechniqueProfileComponent } from './technique-profile/technique-profile.component';
import { TechniqueMonthlyComponent } from './technique-monthly/technique-monthly.component';
import { TechniquePublicdaComponent } from './technique-publicda/technique-publicda.component';
import { TechniqueOfferelementaireComponent } from './technique-offerelementaire/technique-offerelementaire.component';
import { TechniqueBundleofferelementaireComponent } from './technique-bundleofferelementaire/technique-bundleofferelementaire.component';

import { AdminLoginComponent } from './admin-login/admin-login.component';


@NgModule({
  declarations: [
    AppComponent,
    MarketingOffresComponent,
    LogComponent,
    MarketingCreateComponent,
    TechniqueOffresComponent,
    TechniqueUnitComponent,
    NavTechniqueComponent,
    NavMarketingComponent,
    NavAdminComponent,
    TechniqueProfileComponent,
    TechniqueMonthlyComponent,
    TechniquePublicdaComponent,
    AdminOtherserviceComponent,
    TechniqueOfferelementaireComponent,
    TechniqueBundleofferelementaireComponent,
    AdminLoginComponent,
    LogCreateComponent,
    LogUpdateComponent,
    EnteteComponent,
    MarketingOffresRComponent,
    TechniqueOffresRComponent,
   
   
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
   
   
    

  ],
  //FREEAPISERVICE//
  providers: [freeApiService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
