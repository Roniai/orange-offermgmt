import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueProfileComponent } from './technique-profile.component';

describe('TechniqueProfileComponent', () => {
  let component: TechniqueProfileComponent;
  let fixture: ComponentFixture<TechniqueProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
