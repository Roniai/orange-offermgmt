import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { Profile } from '../classes/profile';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-technique-profile',
  templateUrl: './technique-profile.component.html',
  styleUrls: ['./technique-profile.component.css']
})
export class TechniqueProfileComponent implements OnInit {
  roleuser:any="";
  
  lstProfile :Profile[];
  profile:Profile= new Profile();

  isEdit:boolean=false;
  lstIdprofile:any[];
  myIdprofile:any;
  

  constructor( private _freeApiService: freeApiService,private authService: AuthService) { this.lstProfile=[]; this.lstIdprofile=[]; }

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;
    
    ///get
    this._freeApiService.getProfile().subscribe
    (
      data=> {
        this.lstProfile = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
      (
        data=> {
          this.lstIdprofile = data;
        }
      );
  }
  
 onSubmit(form:NgForm):void{
  if(!this.isEdit){

    this._freeApiService.CreateProfile(this.profile).subscribe(
      resp=>{
        console.log(resp);

        this.closeModale();
        
        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'Le nouveau profile est crée avec succès',
          showConfirmButton: false,
          timer: 2500
        })
    
        },
      error1=>{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
          showConfirmButton: false,
          timer: 2500
        })
  
      
    });
  }
  //PUt
    else{
       console.log(form.value);
       this.profile.idprofile = this.myIdprofile ;

      this._freeApiService.UpdateProfile(this.profile).subscribe(
        resp=>{
          console.log(resp);
    
          this.closeModale();

          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'La mise à jour du profile "'+this.profile.idprofile+'" a été bien effectuée',
            showConfirmButton: false,
            timer: 2500
          })
        
            },
          error1=>{
      
            Swal.fire({
              position: 'center',
              icon: 'error',
              text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
              showConfirmButton: false,
              timer: 2500
            })
      });
    }

  
  }
  
  add():void{
    this.isEdit=false;
    this.profile=new Profile;
  }

  //Put(maka value fotsiny )
  edit(data:Profile):void{
    this.isEdit=true;
    this.profile=data;
    this.myIdprofile = this.profile.idprofile;
  }

  //delete
  delete(data:Profile):void{
    Swal.fire({
      title: 'Êtes-vous sûr?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Annuler',
      cancelButtonColor: 'black',
      confirmButtonText: 'Oui, supprime-le!',
      confirmButtonColor: '#ff6600' 

    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'Profile "'+data.idprofile+'" a été supprimé définitivement',
          showConfirmButton: false,
          timer: 2500
         
        })
this._freeApiService.DeleteProfile(data.idprofile).subscribe(
  resp=>{
    this.ngOnInit();
  }
  )
}
})

  }

  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  closeModale(){
    //Fermer modale
    let x = document.getElementById('id01');
    if(x!=null){x.style.display='none'}
    this.ngOnInit();
  }

}

