import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavMarketingComponent } from './nav-marketing.component';

describe('NavMarketingComponent', () => {
  let component: NavMarketingComponent;
  let fixture: ComponentFixture<NavMarketingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavMarketingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
