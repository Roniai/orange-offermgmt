import { Component, OnInit } from '@angular/core';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import {Login} from '../classes/login';

@Component({
  selector: 'app-nav-marketing',
  templateUrl: './nav-marketing.component.html',
  styleUrls: ['./nav-marketing.component.css']
})
export class NavMarketingComponent implements OnInit {
  login:Login;
  trigramme:any="";
  routerTo:any="";

  constructor(private _freeApiService: freeApiService,private authService: AuthService) {this.login=new Login(); }

  ngOnInit(): void {
    //get login  by trigramme
    this.trigramme = this.authService.trigrammeService;

    this._freeApiService.GetOneLoginById(this.trigramme).subscribe
    (
      data=> {
        //Get Login
        this.login = data;

        if(this.login.roleuser=="R"){
          this.routerTo="/Marketing-offres-responsable";
        }
        else{
          this.routerTo="/Marketing-offres";
        }

        console.log(this.login);
    });
  }

}
