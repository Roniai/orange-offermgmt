import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { Monthly } from '../classes/monthly';
import { Offerelementaire} from '../classes/offerelementaire';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-technique-monthly',
  templateUrl: './technique-monthly.component.html',
  styleUrls: ['./technique-monthly.component.css']
})
export class TechniqueMonthlyComponent implements OnInit {
  roleuser:any="";
  
  lstMonthly :Monthly[];
  monthly:Monthly= new Monthly();
  isEdit:boolean=false;

  lstOfferelementaire :Offerelementaire[];

  /* Checkbox */
  lstIsObject:any = {};

  /*List Groups*/ 
  lstGroups:[]=[];
  lstGroups1:[]=[];
  lstGroups2:[]=[];
  lstGroups3:[]=[];

  constructor( private _freeApiService: freeApiService,private authService: AuthService) { this.lstMonthly=[]; this.lstOfferelementaire=[];}

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;
    
    this.lstIsObject["isActive"] = 0;
    
    ///get
    this._freeApiService. getMonthly().subscribe
    (
      data=> {
        this.lstMonthly = data;
      }
    );
    this._freeApiService.getOfferelementaire().subscribe
    (
      data=> {
        this.lstOfferelementaire = data;
      }
    );


    /*List Groups*/
    this._freeApiService. getGroupsMonthly().subscribe
    (
      data=> {
        this.lstGroups = data;
        console.log(this.lstGroups);
      }
    );
    this._freeApiService. getGroups1Monthly().subscribe
    (
      data=> {
        this.lstGroups1 = data;
        console.log(this.lstGroups1);
      }
    );
    this._freeApiService. getGroups2Monthly().subscribe
    (
      data=> {
        this.lstGroups2 = data;
        console.log(this.lstGroups2);
      }
    );
    this._freeApiService. getGroups3Monthly().subscribe
    (
      data=> {
        this.lstGroups3 = data;
        console.log(this.lstGroups3);
      }
    );

    this.hideTextareaGroups();
    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
  }
    
   onSubmit(form:NgForm):void{

     //List Groups
      //Récupérer la valeur du groups textarea
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(this.monthly.groups=="autre"){
        this.monthly.groups = textarea.value;
      }

      //Récupérer la valeur du groups1 textarea1
      var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
        if(this.monthly.groups1=="autre"){
          this.monthly.groups1 = textarea1.value;
        }

      //Récupérer la valeur du groups2 textarea2
      var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
        if(this.monthly.groups2=="autre"){
          this.monthly.groups2 = textarea2.value;
        }

      //Récupérer la valeur du groups3 textarea3
      var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(this.monthly.groups3=="autre"){
          this.monthly.groups3 = textarea3.value;
        }

    if(!this.isEdit){
    console.log(form.value);

    this.monthly.isActive = this.lstIsObject["isActive"];

    console.log("this.monthly = ",this.monthly);
    this._freeApiService.CreateMonthly(this.monthly).subscribe(
      resp=>{
        console.log(resp);
  
        this.closeModale();

        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'La nouvelle monthly est créée avec succès',
          showConfirmButton: false,
          timer: 2500
        })
     
         },
       error1=>{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
          showConfirmButton: false,
          timer: 2500
        })
    });

    }
    //PUt
       else{
         console.log(form.value);

         this.monthly.isActive = this.lstIsObject["isActive"];

         console.log("this.monthly = ",this.monthly);

        this._freeApiService.UpdateMonthly(this.monthly).subscribe(
          resp=>{
            console.log(resp);
      
            this.closeModale();

            Swal.fire({
              position: 'center',
              icon: 'success',
              text: 'La mise à jour de la monthly "'+this.monthly.nom+'" a été bien effectuée',
              showConfirmButton: false,
              timer: 2500
            })
          
              },
            error1=>{
        
              Swal.fire({
                position: 'center',
                icon: 'error',
                text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                showConfirmButton: false,
                timer: 2500
              })
        });

      } 
  
    
    }
    
  add():void{
    this.isEdit=false;
    this.monthly=new Monthly;
    this.lstIsObject["isActive"] = 0;

    this.hideTextareaGroups();
    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
  }

  //Put(maka value fotsiny )
  edit(data:Monthly):void{
    this.isEdit=true;
    this.monthly=data;
    this.lstIsObject["isActive"] = this.monthly.isActive;
  }

  changeValueCheckboxIs(e: any) {
    if(e.target.checked){
      this.lstIsObject[e.target.value] = 1;
    }
    else{
      this.lstIsObject[e.target.value] = 0;
    }
    console.log("this.lstIsObject : ",this.lstIsObject);
  }

  /*List Groups*/
  changeSelectedGroups(e:any){
    //Saisir un nouveau groups hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups(){
    //Cacher textareaGroups
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups1(e:any){
    //Saisir un nouveau groups1 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups1(){
    //Cacher textareaGroups1
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups2(e:any){
    //Saisir un nouveau groups2 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups2(){
    //Cacher textareaGroups2
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
    textarea.style.display='none';
    textarea.value="";
  }
  changeSelectedGroups3(e:any){
    //Saisir un nouveau groups3 hormis de la liste déroulante
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      if(e.target.value=="autre"){
        textarea.style.display='block';
        textarea.value="";
      }
      else{
        textarea.style.display='none';
      }
    
  }
  hideTextareaGroups3(){
    //Cacher textareaGroups3
    var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
    textarea.style.display='none';
    textarea.value="";
  }


  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }

  closeModale(){
    //Fermer modale
    let x = document.getElementById('id01');
    if(x!=null){x.style.display='none'}
    this.ngOnInit();
  }



  //delete
  /* delete(data:Monthly):void{
    Swal.fire({
      text: 'Êtes-vous sûr?',
      text: "Vous ne pourrez plus revenir en arrière!",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Annuler',
      cancelButtonColor: 'black',
      confirmButtonText: 'Oui, supprime-le!',
      confirmButtonColor: '#ff6600' 

    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'Monthly '+data.nom+' a été supprimé définitivement',
          showConfirmButton: false,
          timer: 2500
          
        })
        this._freeApiService.DeleteProfile(data.id).subscribe(
          resp=>{
            this.ngOnInit();
          }
          )
}
})

  } */

}
