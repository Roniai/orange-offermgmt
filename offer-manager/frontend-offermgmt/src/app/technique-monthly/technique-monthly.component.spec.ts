import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueMonthlyComponent } from './technique-monthly.component';

describe('TechniqueMonthlyComponent', () => {
  let component: TechniqueMonthlyComponent;
  let fixture: ComponentFixture<TechniqueMonthlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueMonthlyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueMonthlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
