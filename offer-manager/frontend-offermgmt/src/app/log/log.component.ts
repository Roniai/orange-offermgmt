import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Login } from '../classes/login';
import { freeApiService } from '../services/freeapi.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  login:Login;  
  //SignOut by default
  isAuthStatus:boolean=false;
  /* idlogin:number=0; */
  mdpuser:any=0;

  constructor( private _freeApiService: freeApiService, private authService: AuthService,private router: Router ) {  this.login= new Login();}

  ngOnInit(): void {
    this.isAuthStatus = this.authService.isAuth;
  }

  onSignIn(form:NgForm):void{
    console.log(form.value);
    this._freeApiService.getByTrigramme(this.login).subscribe(
      resp=>{
        console.log(resp);
        console.log("Eny");     
          
        this.login = resp;
        this.authService.trigrammeService = this.login.trigramme;
        this.authService.roleuserService = this.login.roleuser;
        this.authService.depuserService = this.login.depuser;
        this.authService.signIn();
        this.isAuthStatus = this.authService.isAuth;
        
        console.log(this.isAuthStatus);
        console.log(this.authService.trigrammeService);

        if(this.login.activeuser==1){
          if(this.login.depuser=="DMCC" && this.login.roleuser=="US"){
            console.log("Tonga ato am M?");
            this.router.navigate(['Marketing-offres']);
          }
          else if(this.login.depuser=="DMCC" && this.login.roleuser=="R"){
            console.log("Tonga ato am M?");
            this.router.navigate(['Marketing-offres-responsable']);
          }
          else if(this.login.depuser=="DTI" && this.login.roleuser=="US"){
            console.log("Tonga ato am T?");
            this.router.navigate(['Technique-offres']);
          }
          else if(this.login.depuser=="DTI" && this.login.roleuser!="US"){
            console.log("Tonga ato am T?");
            this.router.navigate(['Technique-offres-responsable']);
          }
        }
        else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Désolé, votre compte est désactivé. Veuillez contacter le Superadmin pour l\'activer',
            showConfirmButton: false,
            timer: 3000
          })
        }

      },
      error1=>{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Erreur d\'authentification',
          showConfirmButton: false,
          timer: 2500
        })
      } 
      
     );
  }

  afficheMdpuser(e: any) {
    if(e.target.checked){
      (<HTMLInputElement>document.getElementById("mdpuser")).type = "text";
    }
    else{
      (<HTMLInputElement>document.getElementById("mdpuser")).type = "password";
    }
	  
	}

  //8=backspace, 13=enter
  onlyAlpha(e:any){
    return ((e.charCode >= 65 && e.charCode <= 90) || e.charCode == 8 || e.charCode == 13);
  }

    
}
