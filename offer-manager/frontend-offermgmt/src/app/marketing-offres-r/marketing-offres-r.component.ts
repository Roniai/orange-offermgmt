import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Bundle} from '../classes/bundle';
import { BundleOfferelementaire } from '../classes/bundleofferelementaire';
import { Offerelementaire } from '../classes/offerelementaire';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-marketing-offres-r',
  templateUrl: './marketing-offres-r.component.html',
  styleUrls: ['./marketing-offres-r.component.css']
})
export class MarketingOffresRComponent implements OnInit {

  bundle:Bundle= new Bundle();
  lstBundle :Bundle[];
  data: any = {};

  /* Profilebundle */
  lstprofile:any[];
  listeSelected: any[] = [];
  listIdprofile:any[] = [];
  mylstprofileObject:any = {};

  /* Bundleofferelementaire */
  lstBundleofferelementaire:BundleOfferelementaire[];
  offerelementaire:Offerelementaire = new Offerelementaire();
  lstnature:[]=[];
    /* BundleofferelementaireToUpdate */
  mylstnature:any[]=[];
  mylstquantity:any[]=[];
  mylstunit:any[]=[];
  mylstDA:any[]=[];
  mylstnatureObject:any = {};  

  listeSelectedNature: any[] = [];
  listDA:any[]=[];
  listUnit:any[]=[];
  listQuantity:any[]=[];
  listDAquantity: any = {};

  lstDAToUpdate:any[]=[];
  lstDAToDelete:any[]=[];
  listDAquantityToUpdate: any = {};
  listDAquantityToInsert: any = {};

  /* Checkbox */
  lstIsObject:any = {};

    /*List Groups*/ 
    lstGroups:[]=[];
    lstGroups1:[]=[];
    lstGroups2:[]=[];
    lstGroups3:[]=[];
    lstGroups4:[]=[];
    lstGroups5:[]=[];


  constructor( private _freeApiService: freeApiService) { 
    this.bundle = new Bundle();this.lstBundle=[]; this.lstprofile=[];
    this.lstBundleofferelementaire = [];}

  ngOnInit(): void {
    this.bundle = new Bundle();
    this.listeSelected=[];
    this.mylstprofileObject={};

    //Checkbox is
    this.lstIsObject["isbuylimited"] = 0;
    this.lstIsObject["isBuyableForOther"] = 0;
    this.lstIsObject["isBuyableWithLanyCredit"] = 0;
    this.lstIsObject["isBuyableByOM"] = 0;
    this.lstIsObject["isautorenewable"] = 0;

    ///get
    this._freeApiService. getBundlemk().subscribe
    (
      data=> {
        this.lstBundle = data;
      }
    );
    this._freeApiService. getProfileIdprofile().subscribe
    (
      data=> {
        this.lstprofile = data;
      }
    );
    this._freeApiService. getNature().subscribe
    (
      data=> {
        this.lstnature = data;
      }
    );


    /*List Groups*/
    this._freeApiService. getGroups().subscribe
    (
      data=> {
        this.lstGroups = data;
        console.log(this.lstGroups);
      }
    );
    this._freeApiService. getGroups1().subscribe
    (
      data=> {
        this.lstGroups1 = data;
        console.log(this.lstGroups1);
      }
    );
    this._freeApiService. getGroups2().subscribe
    (
      data=> {
        this.lstGroups2 = data;
        console.log(this.lstGroups2);
      }
    );
    this._freeApiService. getGroups3().subscribe
    (
      data=> {
        this.lstGroups3 = data;
        console.log(this.lstGroups3);
      }
    );
    this._freeApiService. getGroups4().subscribe
    (
      data=> {
        this.lstGroups4 = data;
        console.log(this.lstGroups4);
      }
    );
    this._freeApiService. getGroups5().subscribe
    (
      data=> {
        this.lstGroups5 = data;
        console.log(this.lstGroups5);
      }
    );

    this.hideTextareaGroups();
    this.hideTextareaGroups1();
    this.hideTextareaGroups2();
    this.hideTextareaGroups3();
    this.hideTextareaGroups4();
    this.hideTextareaGroups5();
  }
    
   onSubmit(form:NgForm):void{
      console.log(form.value);

      this.bundle.isbuylimited = this.lstIsObject["isbuylimited"];
      this.bundle.isBuyableForOther = this.lstIsObject["isBuyableForOther"];
      this.bundle.isBuyableWithLanyCredit = this.lstIsObject["isBuyableWithLanyCredit"];
      this.bundle.isBuyableByOM = this.lstIsObject["isBuyableByOM"];
      this.bundle.isautorenewable = this.lstIsObject["isautorenewable"];

      //List Groups
      //Récupérer la valeur du groups textarea
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      if(this.bundle.groups=="autre"){
        this.bundle.groups = textarea.value;
      }

      //Récupérer la valeur du groups1 textarea1
      var textarea1 = (<HTMLTextAreaElement>document.getElementById("textareaGroups1")); 
        if(this.bundle.groups1=="autre"){
          this.bundle.groups1 = textarea1.value;
        }

      //Récupérer la valeur du groups2 textarea2
      var textarea2 = (<HTMLTextAreaElement>document.getElementById("textareaGroups2")); 
        if(this.bundle.groups2=="autre"){
          this.bundle.groups2 = textarea2.value;
        }

      //Récupérer la valeur du groups3 textarea3
      var textarea3 = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(this.bundle.groups3=="autre"){
          this.bundle.groups3 = textarea3.value;
        }

      //Récupérer la valeur du groups4 textarea4
      var textarea4 = (<HTMLTextAreaElement>document.getElementById("textareaGroups4")); 
        if(this.bundle.groups4=="autre"){
          this.bundle.groups4 = textarea4.value;
        }

      //Récupérer la valeur du groups5 textarea5
      var textarea5 = (<HTMLTextAreaElement>document.getElementById("textareaGroups5")); 
        if(this.bundle.groups5=="autre"){
          this.bundle.groups5 = textarea5.value;
        }

      console.log("this.bundle", this.bundle);

      if(this.bundle.isbuylimited==1){
        if(this.bundle.buyfrequency!="" && this.bundle.buymaximum!=0){
          //################Bundle################
          this._freeApiService.UpdateBundleMarketing(this.bundle).subscribe(
            resp=>{
              console.log(resp);

              //################Bundleofferelementaire################

              //Get all quantity and add in array listquantity
              this.listQuantity=[];
              var x = document.querySelectorAll("div.quantity");
              x.forEach(e =>{
                for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                  this.listQuantity.push(e.getElementsByTagName("input")[i].value);
                }
              }); 

              //Turn listDA and listQuantity to Object table associatif (clé -> value)
              for(let i = 0; i < this.listDA.length; i++){
                //inserting new key value pair inside map
                this.listDAquantity[this.listDA[i]] =  this.listQuantity[i];   
                
              };

                console.log(this.listDA);
                console.log(this.listQuantity);
                console.log(this.listDAquantity);
                /* console.log(this.bundle.idbundle); */

                //Initialisation
                this.lstDAToUpdate=[];
                this.lstDAToDelete=[];
                this.listDAquantityToUpdate={};
                this.listDAquantityToInsert={};

                //Distinguer les DA to delete et DAQuantity to update, insert
                for(let i = 0; i < this.mylstDA.length; i++){
                  if(this.listDA.indexOf(this.mylstDA[i])!=-1){
                    //Bundleofferelementaire to update
                    this.lstDAToUpdate.push(this.mylstDA[i]);
    
                    this.listDAquantityToUpdate[this.mylstDA[i]] =  this.listDAquantity[this.mylstDA[i]];
                  }
                  else{
                    //Bundleofferelementaire to delete
                    this.lstDAToDelete.push(this.mylstDA[i]);
                  }
                }
                for(let i = 0; i < this.listDA.length; i++){
                  if(this.lstDAToUpdate.indexOf(this.listDA[i])!=-1){
                    //Bundleofferelementaire to update 
                  }
                  else{
                    //Bundleofferelementaire to insert
                    this.listDAquantityToInsert[this.listDA[i]] =  this.listDAquantity[this.listDA[i]];
                  }
                }

                console.log("List DA New : ",this.listDA);
                console.log("List DA Old : ",this.mylstDA);
                console.log("List DA To update : ",this.lstDAToUpdate);
                console.log("List DA To delete : ",this.lstDAToDelete);
                console.log("List DAQte To update : ",this.listDAquantityToUpdate);
                console.log("List DAQte To insert : ",this.listDAquantityToInsert);

                    /* UPDATE BundleOfferelementaire */
                    this._freeApiService.UpdateBundleofferelementaireMk(this.bundle.idbundle, this.listDAquantityToUpdate).subscribe(
                      resp=>{
                        console.log(resp);
                    });

                    /* INSERT BundleOfferelementaire */
                    this._freeApiService.CreateBundleofferelementaire(this.bundle.idbundle, this.listDAquantityToInsert).subscribe(
                      resp=>{
                        console.log(resp);
                    });

                    /* DELETE BundleOfferelementaire */
                    for(let i = 0; i < this.lstDAToDelete.length; i++){
                      this._freeApiService.DeleteBundleofferelementaireMk(this.bundle.idbundle, this.lstDAToDelete[i]).subscribe(
                        resp=>{
                          console.log(resp);
                      });
                    }

                    //################Profilebundle################
                    
                      //Delete all profile
                      this._freeApiService.DeleteProfilebundleById(this.bundle.idbundle).subscribe(
                        resp=>{
                          console.log(resp);
              
                            //Create new profiles
                            this._freeApiService.CreateProfilebundle(this.bundle.idbundle , this.listeSelected).subscribe(
                              res => {
                            
                              Swal.fire({
                                position: 'center',
                                icon: 'success',
                                text: 'La mise à jour de l\'offre "'+this.bundle.nom+'" a été bien effectuée',
                                showConfirmButton: false,
                                timer: 2500
                              })
                              
                              this.closeModale();   
                          
                              },
                              error1=>{
                                  Swal.fire({
                                    position: 'center',
                                    icon: 'error',
                                    title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                                    showConfirmButton: false,
                                    timer: 2500
                                  })
                            });
                      });
                    
          });
        }
        else{
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Vous venez de cliquer sur "isBuylimited", alors vous devez préciser "Buyfrequency" et "Buymaximum"',
            showConfirmButton: false,
            timer: 5000
          })
        }
      }
      else{
        //################Bundle################
        this._freeApiService.UpdateBundleMarketing(this.bundle).subscribe(
          resp=>{
            console.log(resp);

            //################Bundleofferelementaire################

            //Get all quantity and add in array listquantity
            this.listQuantity=[];
            var x = document.querySelectorAll("div.quantity");
            x.forEach(e =>{
              for (let i = 0; i < e.getElementsByTagName("input").length ; i++){
                this.listQuantity.push(e.getElementsByTagName("input")[i].value);
              }
            }); 

            //Turn listDA and listQuantity to Object table associatif (clé -> value)
            for(let i = 0; i < this.listDA.length; i++){
              //inserting new key value pair inside map
              this.listDAquantity[this.listDA[i]] =  this.listQuantity[i];   
              
            };

              console.log(this.listDA);
              console.log(this.listQuantity);
              console.log(this.listDAquantity);
              /* console.log(this.bundle.idbundle); */

              //Initialisation
              this.lstDAToUpdate=[];
              this.lstDAToDelete=[];
              this.listDAquantityToUpdate={};
              this.listDAquantityToInsert={};

              //Distinguer les DA to delete et DAQuantity to update, insert
              for(let i = 0; i < this.mylstDA.length; i++){
                if(this.listDA.indexOf(this.mylstDA[i])!=-1){
                  //Bundleofferelementaire to update
                  this.lstDAToUpdate.push(this.mylstDA[i]);
  
                  this.listDAquantityToUpdate[this.mylstDA[i]] =  this.listDAquantity[this.mylstDA[i]];
                }
                else{
                  //Bundleofferelementaire to delete
                  this.lstDAToDelete.push(this.mylstDA[i]);
                }
              }
              for(let i = 0; i < this.listDA.length; i++){
                if(this.lstDAToUpdate.indexOf(this.listDA[i])!=-1){
                  //Bundleofferelementaire to update 
                }
                else{
                  //Bundleofferelementaire to insert
                  this.listDAquantityToInsert[this.listDA[i]] =  this.listDAquantity[this.listDA[i]];
                }
              }

              console.log("List DA New : ",this.listDA);
              console.log("List DA Old : ",this.mylstDA);
              console.log("List DA To update : ",this.lstDAToUpdate);
              console.log("List DA To delete : ",this.lstDAToDelete);
              console.log("List DAQte To update : ",this.listDAquantityToUpdate);
              console.log("List DAQte To insert : ",this.listDAquantityToInsert);

                  /* UPDATE BundleOfferelementaire */
                  this._freeApiService.UpdateBundleofferelementaireMk(this.bundle.idbundle, this.listDAquantityToUpdate).subscribe(
                    resp=>{
                      console.log(resp);
                  });

                  /* INSERT BundleOfferelementaire */
                  this._freeApiService.CreateBundleofferelementaire(this.bundle.idbundle, this.listDAquantityToInsert).subscribe(
                    resp=>{
                      console.log(resp);
                  });

                  /* DELETE BundleOfferelementaire */
                  for(let i = 0; i < this.lstDAToDelete.length; i++){
                    this._freeApiService.DeleteBundleofferelementaireMk(this.bundle.idbundle, this.lstDAToDelete[i]).subscribe(
                      resp=>{
                        console.log(resp);
                    });
                  }

                  //################Profilebundle################
                  
                    //Delete all profile
                    this._freeApiService.DeleteProfilebundleById(this.bundle.idbundle).subscribe(
                      resp=>{
                        console.log(resp);
            
                          //Create new profiles
                          this._freeApiService.CreateProfilebundle(this.bundle.idbundle , this.listeSelected).subscribe(
                            res => {
                          
                            Swal.fire({
                              position: 'center',
                              icon: 'success',
                              text: 'La mise à jour de l\'offre "'+this.bundle.nom+'" a été bien effectuée',
                              showConfirmButton: false,
                              timer: 2500
                            })
                            
                            this.closeModale();   
                        
                            },
                            error1=>{
                                Swal.fire({
                                  position: 'center',
                                  icon: 'error',
                                  title: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
                                  showConfirmButton: false,
                                  timer: 2500
                                })
                          });
                    });
                  
        });
      }
      

    }
   
    
    //Put(maka value fotsiny )
    edit(data:Bundle):void{
      this.bundle=data;

      this.lstIsObject["isbuylimited"] = this.bundle.isbuylimited;
      this.lstIsObject["isBuyableForOther"] = this.bundle.isBuyableForOther;
      this.lstIsObject["isBuyableWithLanyCredit"] = this.bundle.isBuyableWithLanyCredit;
      this.lstIsObject["isBuyableByOM"] = this.bundle.isBuyableByOM;
      this.lstIsObject["isautorenewable"] = this.bundle.isautorenewable;

      //isBuyLimited is checked => enable buyfrequence et buymaximum
      if(this.bundle.isbuylimited==1){
        this.isBuyLimitedChecked(true);
      }
      else{
        this.isBuyLimitedChecked(false);
      }

      //Profilebundle
        this._freeApiService.GetIdprofileofBundle(data.idbundle).subscribe(
          resp=>{
            this.listIdprofile = resp;

            //Initialisation
            this.listeSelected=[];
            this.mylstprofileObject={};

              for (let i = 0; i < this.lstprofile.length ; i++){
                //Si l'élément existe déjà
                if(this.listIdprofile.indexOf(this.lstprofile[i])!=-1){
                    //Tableau associatif pour stocker "checked=true or false"
                    this.mylstprofileObject[this.lstprofile[i]] = true;

                    this.listeSelected.push(this.lstprofile[i]);
                    
                }
                else{
                  this.mylstprofileObject[this.lstprofile[i]] = false;
                }

              }

              //Actualiser lstprofile pour refaire *ngFor="let profile of lstprofile"
              this.lstprofile=[];
              this.refreshListeProfile();

              console.log("this.mylstprofileObject : ",this.mylstprofileObject);
              console.log("this.listeSelected : ",this.listeSelected);
        });

      //Bundleofferelemenataire
        this._freeApiService.getBundleofferelementaireByIdbundle(data.idbundle).subscribe(
          resp=>{
            this.lstBundleofferelementaire = resp;
              
              //Initialisation
              let that = this;
              that.mylstnature=[];
              that.mylstunit=[];
              that.mylstDA=[];
              that.mylstquantity=[];
              that.mylstnatureObject={};
              that.listeSelectedNature=[];
              that.listUnit=[];
              that.listDA=[];
              that.listQuantity=[];

              let myPromise = new Promise(function(myResolve, myReject) {
                for (let i = 0; i < that.lstBundleofferelementaire.length ; i++){

                  that._freeApiService.GetOneOfferelementaire(that.lstBundleofferelementaire[i].DA).subscribe(res => {
                    that.offerelementaire = res;
                    console.log("offerelementaire = ",that.offerelementaire);

                    that.mylstnature.push(that.offerelementaire.nature);
                    that.mylstunit.push(that.offerelementaire.unit);
                    that.mylstquantity.push(that.lstBundleofferelementaire[i].quantity);
                    that.mylstDA.push(that.lstBundleofferelementaire[i].DA);


                    //Appel la fonction resolve quand cette boucle est terminée
                    if(that.mylstnature.length == that.lstBundleofferelementaire.length) {
                      myResolve("Function completed");
                    }
                  
                  });  
                  
                }
                //Pas encore de offerelementaire
                if(that.lstBundleofferelementaire.length==0) {
                  myResolve("Function completed");
                }
                
              });
              
              myPromise.then(function(value) {
                /* console.log(value); */
                console.log("nature ivelany= ",that.mylstnature);
                console.log("unit ivelany= ",that.mylstunit);
                console.log("DA ivelany= ",that.mylstDA);
                console.log("quantity ivelany= ",that.mylstquantity);

                for (let i = 0; i < that.lstnature.length ; i++){
    
                  //Si l'élément existe déjà
                  if(that.mylstnature.indexOf(that.lstnature[i])!=-1){
                    /* ########## TD LEFT ########## */

                      //Tableau associatif pour stocker "checked=true or false"
                      that.mylstnatureObject[that.lstnature[i]] = true;

                    /* ########## TD RIGHT ########## */

                      /* Nature selected */
                      that.listeSelectedNature.push(that.lstnature[i]);

                      let indexOf = that.mylstnature.indexOf(that.lstnature[i]);

                        /* DA */
                        that.listDA.push(that.mylstDA[indexOf]);

                        /* Quantity */
                        that.listQuantity.push(that.mylstquantity[indexOf]);

                        /* Unit */  

                        if(that.mylstunit[indexOf]==1){
                          that.listUnit.push("SMS");
                        }
                        else if(that.mylstunit[indexOf]==2){
                          that.listUnit.push("Ariary");
                        }
                        else if(that.mylstunit[indexOf]==3){
                          that.listUnit.push("Seconde");
                        }
                        else if(that.mylstunit[indexOf]==4){
                          that.listUnit.push("Octet");
                        }
                        else{
                          that.listUnit.push("Unité inconnue");
                        }

                  }
                  else{
                    that.mylstnatureObject[that.lstnature[i]] = false;
                  }

                }
                console.log('Selected nature : ' , that.listeSelectedNature);
                console.log('DA : ' , that.listDA);
                console.log('Unit : ' , that.listUnit);
                console.log('Quantité : ' , that.listQuantity);

                //Actualiser lstnature pour refaire *ngFor="let nature of lstnature"
                that.lstnature=[];
                that.refreshListeNature();

              });

        });
    }


    changeValueCheckbox(e: any) {
      if(e.target.checked){
        console.log(e.target.value + 'Checked')
        this.listeSelected.push(e.target.value);
        console.log(this.listeSelected);
      }
      else{
        this.listeSelected=this.listeSelected.filter(m=>m!=e.target.value);
        console.log(e.target.value + 'UNChecked');
        console.log(this.listeSelected);
      }
      
    }
    changeValueCheckboxNature(e: any) {
     
      if(e.target.checked){
  
        this.listeSelectedNature.push(e.target.value);
        this._freeApiService.getDAofNature(e.target.value).subscribe(res => {
          this.listDA.push(res);
          /* console.log("listDA getDAofNat : ",this.listDA ); */
    
        },error => {
          console.log(error);
        });
    
        /* this.listUnit=[]; */
        this._freeApiService.getUnitofNature(e.target.value).subscribe(res => {
          /* for(let i = 0; i < res.length; i++){
            if(res[i]=="1"){
              this.listUnit.push("SMS");
            }
            else if(res[i]=="2"){
              this.listUnit.push("Ariary");
            }
            else if(res[i]=="3"){
              this.listUnit.push("Seconde");
            }
            else if(res[i]=="4"){
              this.listUnit.push("Octet");
            }
            else{
              this.listUnit.push("Unité inconnue");
            }
            
          } */
          if(res=="1"){
            this.listUnit.push("SMS");
          }
          else if(res=="2"){
            this.listUnit.push("Ariary");
          }
          else if(res=="3"){
            this.listUnit.push("Seconde");
          }
          else if(res=="4"){
            this.listUnit.push("Octet");
          }
          else{
            this.listUnit.push("Unité inconnue");
          }
          /* console.log(this.listUnit); */
    
        },error => {
          console.log(error);
        });
    
        console.log('listeSelectedNature vao2 : ', this.listeSelectedNature,e.target.value + ' Checked');
        console.log("listDA vao2 : ",this.listDA );
        console.log("listUnit vao2 : ",this.listUnit);
        console.log("listQuantity vao2 : ",this.listQuantity);  
  
      }
  
      else{
        //On a decoché une nature qui a déjà un unit et quantity dans la BD (to update)
        let indexOf = this.listeSelectedNature.indexOf(e.target.value);
        this.listUnit.splice(indexOf,1);
        this.listQuantity.splice(indexOf,1);
        this.listDA.splice(indexOf,1);

        this.listeSelectedNature.splice(indexOf,1);   
  
        console.log('listeSelectedNature sisa : ', this.listeSelectedNature,e.target.value + ' UNChecked');
        console.log("listDA sisa : ",this.listDA );
        console.log("listUnit sisa : ",this.listUnit);
        console.log("listQuantity sisa : ",this.listQuantity);  
         
      }
      
    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;
  
        if(e.target.value == "isbuylimited"){
          this.isBuyLimitedChecked(true);
        }
      }
      else{
        this.lstIsObject[e.target.value] = 0;
        
        if(e.target.value == "isbuylimited"){
          this.isBuyLimitedChecked(false);
        }
      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }
    isBuyLimitedChecked(isChecked:boolean){
      var x = document.querySelectorAll('div.buyLimitedChecked');
      if(isChecked==true){
        x.forEach(e=>{
          for(let i=0; i<e.getElementsByTagName('input').length; i++){
            e.getElementsByTagName('input')[i].disabled=false;
          }
        });
      }
      else{
        x.forEach(e=>{
          for(let i=0; i<e.getElementsByTagName('input').length; i++){
            e.getElementsByTagName('input')[i].disabled=true;
          }
        });
        this.bundle.buyfrequency="";
        this.bundle.buymaximum=0;
      }
      
  
    }

    refreshListeProfile(){
      this._freeApiService. getProfileIdprofile().subscribe
      (
        data=> {
          this.lstprofile = data;
        }
      );
    }
    refreshListeNature(){
      this._freeApiService. getNature().subscribe
      (
        data=> {
          this.lstnature = data;
        }
      );
    }

   


      /*List Groups*/
    changeSelectedGroups(e:any){
      //Saisir un nouveau groups hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups(){
      //Cacher textareaGroups
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups1(e:any){
      //Saisir un nouveau groups1 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups1(){
      //Cacher textareaGroups1
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups1"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups2(e:any){
      //Saisir un nouveau groups2 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups2(){
      //Cacher textareaGroups2
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups2"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups3(e:any){
      //Saisir un nouveau groups3 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups3(){
      //Cacher textareaGroups3
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups3"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups4(e:any){
      //Saisir un nouveau groups4 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups4(){
      //Cacher textareaGroups4
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups4"));
      textarea.style.display='none';
      textarea.value="";
    }
    changeSelectedGroups5(e:any){
      //Saisir un nouveau groups5 hormis de la liste déroulante
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
        if(e.target.value=="autre"){
          textarea.style.display='block';
          textarea.value="";
        }
        else{
          textarea.style.display='none';
        }
      
    }
    hideTextareaGroups5(){
      //Cacher textareaGroups5
      var textarea = (<HTMLTextAreaElement>document.getElementById("textareaGroups5"));
      textarea.style.display='none';
      textarea.value="";
    }


    onlyInteger(e:any){
      return e.charCode >= 48 && e.charCode <= 57;
    }
    
    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }

   

}
