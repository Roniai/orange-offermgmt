import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingOffresRComponent } from './marketing-offres-r.component';

describe('MarketingOffresRComponent', () => {
  let component: MarketingOffresRComponent;
  let fixture: ComponentFixture<MarketingOffresRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarketingOffresRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingOffresRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
