export class BundleOfferelementaire
{
    
    DA?: number;
    idbundle?:number;
    ismixed:number;
    istoexcludefrominfoconso: number;
    offerId?: number;
    quantity: number;
    tarifappelfamille: number;
    tarifappelinternational: number;
    tarifappelnat:number;
    tarifappelorange: number;
    tarifdata: number;
    tarifdatafb:number;
    tarifsmsfamille: number;
    tarifsmsinternational:number;
    tarifsmsnat: number;
    tarifsmsorange: number;
    
 

    constructor(){
     this.DA=0;
     this.idbundle=0;
     this.ismixed=0;
     this.istoexcludefrominfoconso=0;
     this.offerId=0;
     this.quantity=0;
     this.tarifappelfamille=0;
     this.tarifappelinternational=0;
     this.tarifappelnat=0;
     this.tarifappelorange=0;
     this.tarifdata=0;
     this.tarifdatafb=0;
     this.tarifsmsfamille=0;
     this.tarifsmsinternational=0;
     this.tarifsmsnat=0;
     this.tarifsmsorange=0;
     
    
    }
  }