
export class Profile 
{
    communityid: string;
    groupeid: string;
    idprofile?: string;
    lcg:number; 
    lcgref: number; 
    lcint: number; 
    lcintref: number; 
    lcn: number; 
    lcnref: number; 
    lcr: number; 
    lcrref: number; 
    parent: string;
    serviceclass: string;
    title: string;
    ucg: number;
    ucint: number;
    ucn: number;
    ucr: number;

    constructor(){
       this.communityid="";
       this.groupeid="";
    this.idprofile="";
    this.lcg=0; 
   this. lcgref= 0; 
    this.lcint= 0; 
    this.lcintref= 0; 
    this.lcn= 0; 
    this.lcnref= 0; 
    this.lcr= 0; 
    this.lcrref= 0; 
    this.parent="";
    this.serviceclass= "";
    this.title= "";
    this.ucg= 0;
    this.ucint= 0;
    this.ucn= 0;
    this.ucr= 0;
    }
  }