export class BundleConfig 
{
    
  accumulatorid:number;
  
  chargingindicator: number;
  chargingindicatorom: number;
 
  isroaming: number;
  isActive:number;
  pamclass: number;
  pamscheduled:number;
  pamservice: number;
  
  refill: string;
  refillapi: string;
  refillfwd: string;
  refillinjection: string
  refilllanycredit: string;
  refilllms: string;
  refillmypos: string;
  refillom: string;
  
  
  buyfrequency: string;
  buymaximum: number;
  commentaire: string;
  description: string;
  generalrank: number;
  grouprank: number;
  groups: string;
  groups1: string;
  groups2: string;
  groups3: string;
  groups4: string;
  groups5: string;
  groupsrank1: number;
  groupsrank2: number;
  groupsrank3: number;
  groupsrank4: number;
  groupsrank5: number;
  idbundle?: any;
  isBuyableByOM: number;
  isBuyableForOther: number;
  isBuyableWithLanyCredit: number;
  isautorenewable: number;
  isbuylimited: number;
  nom: string;
  prix: number;
  taxe:string;
  validite: string;
 
  

    constructor(){
      this.accumulatorid=0;
      this.chargingindicator=0;
      this.chargingindicatorom=0;
     
    
     
     
      this.isroaming=0;
      this.isActive=0;
      this.pamclass=0;
      this.pamscheduled=0;
      this.pamservice=0;
      
      this.refill="";
      this.refillapi="";
      this.refillfwd="";
      this.refillinjection="";
      this.refilllanycredit="";
      this.refilllms="";
      this.refillmypos="";
      this.refillom="";
     this.buyfrequency="";
     this.buymaximum=0;
     this.commentaire="";
     this.description="";
     this.generalrank=0;
     this.grouprank=0;
     this.grouprank=0;
     this.groups="";
     this.groups1="";
     this.groups2="";
     this.groups3="";
     this.groups4="";
     this.groups5="";
     this.groupsrank1=0;
     this.groupsrank2=0;
     this.groupsrank3=0;
     this.groupsrank4=0;
     this.groupsrank5=0;
     this.isBuyableByOM=0;
     this.isBuyableForOther=0;
     this.isBuyableWithLanyCredit=0;
     this.isautorenewable=0;
     this.isbuylimited=0;
     this.nom="";
     this.prix=0;
     this.taxe="";
     this.validite="";
     
    
    }
  }