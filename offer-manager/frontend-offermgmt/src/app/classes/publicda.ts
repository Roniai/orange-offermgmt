export class Publicda
{
    
   DA?: number;
   groups1: string;
   groups2: string;
   groups3: string;
   groupsrank1: number;
   groupsrank2: number;
   groupsrank3: number;
   idprofile?: string;
   isActive: number;
   ismixed: number;
   isroaming: number;
   istoexcludefrominfoconso: number;
   nom: string;
   tarifappelfamille: number
   tarifappelinternational: number;
   tarifappelnat: number;
   tarifappelorange: number;
   tarifdata: number;
   tarifsmsfamille: number;
   tarifsmsinternational: number;
   tarifsmsnat: number;
   tarifsmsorange: number;

    constructor(){
     this.DA=0;
     this.groups1="";
     this.groups2="";
     this.groups3="";
     this.groupsrank1=-1;
     this.groupsrank2=-1;
     this.groupsrank3=-1;
    this.idprofile= "";
     this.isActive= 0;
     this.ismixed= 0;
     this.isroaming= 0;
     this.istoexcludefrominfoconso= 0;
     this.nom= "";
     this.tarifappelfamille= 0;
     this.tarifappelinternational= 0;
     this.tarifappelnat= 0;
     this.tarifappelorange=0;
     this.tarifdata=0;
     this.tarifsmsfamille=0 ;
     this.tarifsmsinternational=0;
     this.tarifsmsnat=0;
     this.tarifsmsorange=0;
    
    }
  }