export class Monthly 
{
    
   da: number;
  groups: string;
  groups1: string;
  groups2: string;
  groups3: string;
  groupsrank: number;
  groupsrank1: number;
  groupsrank2: number;
  groupsrank3: number;
  id?: any;
  isActive: number;
  nom: string;
  offerID: number;

    constructor(){
     this.da=0;
     this.groups="";
     this.groups1="";
     this.groups2="";
     this.groups3="";
     this.groupsrank=0;
     this.groupsrank1=-1;
     this.groupsrank2=-1;
     this.groupsrank3=-1;
     this.isActive=0;
     this.nom="";
     this.offerID=-1;
    }
  }