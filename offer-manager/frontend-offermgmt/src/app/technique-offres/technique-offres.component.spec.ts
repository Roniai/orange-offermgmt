import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueOffresComponent } from './technique-offres.component';

describe('TechniqueOffresComponent', () => {
  let component: TechniqueOffresComponent;
  let fixture: ComponentFixture<TechniqueOffresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueOffresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueOffresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
