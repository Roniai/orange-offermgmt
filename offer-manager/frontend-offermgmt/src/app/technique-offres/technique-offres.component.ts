import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { BundleConfig} from '../classes/bundleconfig';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-technique-offres',
  templateUrl: './technique-offres.component.html',
  styleUrls: ['./technique-offres.component.css']
})
export class TechniqueOffresComponent implements OnInit {
  bundle:BundleConfig= new BundleConfig();
  lstBundleConfig :BundleConfig[];

  /* Checkbox */
  lstIsObject:any = {};


  constructor( private _freeApiService: freeApiService) { this.lstBundleConfig=[];} 
    

  ngOnInit(): void {

    this.bundle = new BundleConfig();

    //Checkbox is
    this.lstIsObject["isbuylimited"] = 0;
    this.lstIsObject["isBuyableForOther"] = 0;
    this.lstIsObject["isBuyableWithLanyCredit"] = 0;
    this.lstIsObject["isBuyableByOM"] = 0;
    this.lstIsObject["isautorenewable"] = 0;

    //Technique
    this.lstIsObject["isActive"] = 0;
    this.lstIsObject["isroaming"] = 0;

    ///get
    this._freeApiService. getBundle().subscribe
    (
      data=> {
        this.lstBundleConfig = data;
      }
    );
    }

    
  onSubmit(form:NgForm):void{
      console.log(form.value);

      this.bundle.isbuylimited = this.lstIsObject["isbuylimited"];
      this.bundle.isBuyableForOther = this.lstIsObject["isBuyableForOther"];
      this.bundle.isBuyableWithLanyCredit = this.lstIsObject["isBuyableWithLanyCredit"];
      this.bundle.isBuyableByOM = this.lstIsObject["isBuyableByOM"];
      this.bundle.isautorenewable = this.lstIsObject["isautorenewable"];

      //Technique
      this.bundle.isActive = this.lstIsObject["isActive"];
      this.bundle.isroaming = this.lstIsObject["isroaming"];
      
      console.log(this.bundle);

      //################Bundle################
      this._freeApiService.UpdateBundle(this.bundle).subscribe(
        resp=>{
          console.log(resp);

          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'La mise à jour de l\'offre "'+this.bundle.nom+'" a été bien effectuée',
            showConfirmButton: false,
            timer: 2500
          })
          this.closeModale();
        
            },
          error1=>{
      
            Swal.fire({
              position: 'center',
              icon: 'error',
              text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau à nouveau',
              showConfirmButton: false,
              timer: 2500
            })
                
      });

  }

    edit(data:BundleConfig):void{
  
      this.bundle=data;

      this.lstIsObject["isbuylimited"] = this.bundle.isbuylimited;
      this.lstIsObject["isBuyableForOther"] = this.bundle.isBuyableForOther;
      this.lstIsObject["isBuyableWithLanyCredit"] = this.bundle.isBuyableWithLanyCredit;
      this.lstIsObject["isBuyableByOM"] = this.bundle.isBuyableByOM;
      this.lstIsObject["isautorenewable"] = this.bundle.isautorenewable;


      //isBuyableForOther
      var input = (<HTMLInputElement>document.getElementById("refillfwd"));
      if(this.bundle.isBuyableForOther==1){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refillfwd="";
      }

      //isautorenewable
      var x = (<HTMLDivElement>document.getElementById('Pam'));
      if(this.bundle.isautorenewable==1){
        for(let i=0; i<x.getElementsByTagName('input').length; i++){
          x.getElementsByTagName('input')[i].disabled=false;
        }
      }
      else{
        for(let i=0; i<x.getElementsByTagName('input').length; i++){
          x.getElementsByTagName('input')[i].disabled=true;
        }
        this.bundle.pamclass=0;
        this.bundle.pamscheduled=0;     
        this.bundle.pamservice=0; 
      }

      //isBuyableByOM
      var input = (<HTMLInputElement>document.getElementById("refillom"));
      if(this.bundle.isBuyableByOM==1){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refillom="";
      }

      //isBuyableWithLanyCredit
      var input = (<HTMLInputElement>document.getElementById("refilllanycredit"));
      if(this.bundle.isBuyableWithLanyCredit==1){
        input.disabled=false;
      }
      else{
        input.disabled=true;
        this.bundle.refilllanycredit="";
      }
      
      //Technique Level
      this.lstIsObject["isActive"] = this.bundle.isActive;
      this.lstIsObject["isroaming"] = this.bundle.isroaming;

    }

    changeValueCheckboxIs(e: any) {
      if(e.target.checked){
        this.lstIsObject[e.target.value] = 1;
      }
      else{
        this.lstIsObject[e.target.value] = 0;
      }
      console.log("this.lstIsObject : ",this.lstIsObject);
    }
   
    onlyInteger(e:any){
      return e.charCode >= 48 && e.charCode <= 57;
    }
     
    closeModale(){
      //Fermer modale
      let x = document.getElementById('id01');
      if(x!=null){x.style.display='none'}
      this.ngOnInit();
    }

  }
