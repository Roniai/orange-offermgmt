import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import {Login} from '../classes/login';

@Component({
  selector: 'app-log-create',
  templateUrl: './log-create.component.html',
  styleUrls: ['./log-create.component.css']
})
export class LogCreateComponent{
  
    login:Login;
  
  constructor( private _freeApiService: freeApiService,private router: Router ) { this.login= new Login(); }
      
    //--------CREATE COMPTE-----------/
    
  onSubmit(form:NgForm):void{
    console.log(this.login);

    //Par défaut "Utilisateur Simple"
    this.login.roleuser = "US";
    console.log(this.login.trigramme.length);

    if(this.login.trigramme!="" && this.login.trigramme.length==3){
      if(this.login.depuser!=""){
        this._freeApiService.CreateLogin(this.login).subscribe(
          resp=>{
            console.log(resp);
            console.log("Eny");
  
             let that = this;
  
            let myPromise = new Promise(function(myResolve, myReject){
              Swal.fire({
                position: 'center',
                icon: 'success',
                text: 'Votre demande de création de nouveau compte a été envoyé, veuillez attendre la décision de Superadmin pour activer votre compte',
                showConfirmButton: false,
                timer: 5000
              })
              setTimeout(function(){myResolve("finished"),5500});
            });
  
            myPromise.then(function(value) {
              that.router.navigate(['']); 
            });
  
        
        },
        error1=>{
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
            showConfirmButton: false,
            timer: 2500
          })
        });
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Veuillez choisir votre département s\'il vous plaît',
          showConfirmButton: false,
          timer: 2000
        })
      }
      
    }
    else{
      Swal.fire({
        position: 'center',
        icon: 'error',
        text: 'Votre trigramme n\'est pas correct',
        showConfirmButton: false,
        timer: 2000
      })
    }    
       
  }

  //8=backspace, 13=enter
  onlyAlpha(e:any){
    return ((e.charCode >= 65 && e.charCode <= 90) || e.charCode == 8 || e.charCode == 13);
  }

}
