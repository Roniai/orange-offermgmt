import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Unit } from '../classes/unit';
import { Profile } from '../classes/profile';
import { Monthly } from '../classes/monthly';
import { Publicda } from '../classes/publicda';
import { Otherservice } from '../classes/otherservice';
import { Offerelementaire} from '../classes/offerelementaire';
import { Bundle} from '../classes/bundle';
import { BundleConfig} from '../classes/bundleconfig';
import { BundleOfferelementaire} from '../classes/bundleofferelementaire';
import { Login } from '../classes/login';

@Injectable()
export class freeApiService {
   constructor(private httpclient:HttpClient) {}

   //#################### Unit ####################

   getUnit(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/unit");
   }

   CreateUnit(data:Unit): Observable<any> {
      return this.httpclient.post("http://localhost:8080/unit",data);
   }

   UpdateUnit(data:Unit): Observable<any> {
      return this.httpclient.put("http://localhost:8080/unit/"+data.unit,data);
   }

   DeleteUnit(unit:any): Observable<any> {
      return this.httpclient.delete("http://localhost:8080/unit/"+unit);
   }      


   //#################### Profile ####################

   getProfile(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/profile")
   }
   getProfileIdprofile(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/profile/idprofile")
   }

   CreateProfile(data:Profile): Observable<any> {
      return this.httpclient.post("http://localhost:8080/profile",data);
   }

   UpdateProfile(data:Profile): Observable<any> {
      return this.httpclient.put("http://localhost:8080/profile/"+data.idprofile,data);
   }

   GetOneProfile(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/profile/"+id);
   }

   DeleteProfile(idprofile:any): Observable<any> {
      return this.httpclient.delete("http://localhost:8080/profile/"+idprofile);
   }  
    
   
   //#################### Monthly ####################

   getMonthly(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/monthly")
   }
   
   CreateMonthly(data:Monthly): Observable<Monthly> {
      return this.httpclient.post<Monthly>("http://localhost:8080/monthly",data);
    }

   UpdateMonthly(data:Monthly): Observable<any> {
      return this.httpclient.put("http://localhost:8080/monthly/"+data.id,data);
   }

   GetOneMonthly(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/monthly/"+id);
   }

   //Get groups monthly
   getGroupsMonthly(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/monthly/groups");
   }

   getGroups1Monthly(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/monthly/groups1");
   }

   getGroups2Monthly(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/monthly/groups2");
   }

   getGroups3Monthly(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/monthly/groups3");
   }


   //#################### Publicda ####################

   getPublicda(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/publicda")
   }

   CreatePublicda(data:Publicda): Observable<Monthly> {
      return this.httpclient.post<Monthly>("http://localhost:8080/publicda",data);
   }

   UpdatePublicda(data:Publicda): Observable<any> {
   return this.httpclient.put("http://localhost:8080/publicda/"+data.idprofile+"/"+data.DA,data);
   }

   GetOnePublicda(id:any): Observable<any> {
   return this.httpclient.get("http://localhost:8080/publicda/"+id);
   }

   //Get groups monthly
   getGroups1Publicda(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/publicda/groups1");
   }

   getGroups2Publicda(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/publicda/groups2");
   }

   getGroups3Publicda(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/publicda/groups3");
   }


   //#################### Offerelementaire ####################

   getOfferelementaire(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/offerelementaire");
   }
   
   getOfferelementaireNature(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/offerelementaire/nature");
   }
   
   getOfferelementaireNatureParam(data:Offerelementaire): Observable<any>{
      return this.httpclient.get("http://localhost:8080/offerelementaire/"+data.nature);
   }

   CreateOfferelementaire(data:Offerelementaire): Observable<any> {
      return this.httpclient.post("http://localhost:8080/offerelementaire",data);
   }
      
   UpdateOfferelementaire(data:Offerelementaire): Observable<any> {
      return this.httpclient.put("http://localhost:8080/offerelementaire/"+data.DA,data);
   }

   DeleteOfferelementaire(id:any): Observable<any> {
      return this.httpclient.delete("http://localhost:8080/offerelementaire/"+id);
   }  
  
   
   //#################### Otherservice ####################

   getOtherservice(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/otherservice")
   }

   CreateOtherservice(data:Otherservice): Observable<Otherservice> {
      return this.httpclient.post<Otherservice>("http://localhost:8080/otherservice",data);
    }

   UpdateOtherservice(data:Otherservice): Observable<any> {
      return this.httpclient.put("http://localhost:8080/otherservice/"+data.idotherservice,data);
   }

   GetOneOtherservice(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/otherservice/"+id);
   }

   //Get groups otherservice
   getGroupsOtherservice(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/otherservice/groups");
   }

   getGroups1Otherservice(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/otherservice/groups1");
   }

   getGroups2Otherservice(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/otherservice/groups2");
   }

   getGroups3Otherservice(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/otherservice/groups3");
   }


   //#################### Profileotherservice ####################

   CreateProfileotherservice( idotherservice?:any, idprofile?:string[]): Observable<any> {
      console.log('url : http://localhost:8080​/profileotherservice/' + idotherservice)
      return this.httpclient.post("http://localhost:8080/profileotherservice/" + idotherservice, idprofile);
   }

   GetIdprofileofOtherservice(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/profileotherservice/getidprofile/"+id);
   }

   DeleteProfileotherserviceById(id:any): Observable<any> {
      console.log("http://localhost:8080/profileotherservice/"+id);
      return this.httpclient.delete("http://localhost:8080/profileotherservice/"+id);
   }


   //#################### Bundle ####################

   //Marketing create

   CreateBundle(data:Bundle): Observable<Bundle> {
      return this.httpclient.post<Bundle>("http://localhost:8080/bundle/mk",data);
    }

   getBundlemk(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/mk");
   }

   UpdateBundleMarketing(data:Bundle): Observable<any> {
      return this.httpclient.put("http://localhost:8080/bundle/mk/"+data.idbundle,data);
   }

   // Technique Bundle 

   getBundle(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle");
   }

   GetOneBundle(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/bundle/"+id);
   }
      
   UpdateBundle(data:BundleConfig): Observable<any> {
      return this.httpclient.put("http://localhost:8080/bundle/"+data.idbundle,data);
   }

   UpdateBundleMkTc(data:BundleConfig): Observable<any> {
      return this.httpclient.put("http://localhost:8080/bundle/mktc/"+data.idbundle,data);
   }

   //Get groups bundle
   getGroups(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups");
   }

   getGroups1(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups1");
   }

   getGroups2(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups2");
   }

   getGroups3(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups3");
   }

   getGroups4(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups4");
   }

   getGroups5(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/groups5");
   }


   //#################### Profilebundle ####################

   CreateProfilebundle( idbundle?:any, idprofile?:any[]): Observable<any> {
      console.log('url : http://localhost:8080​/profilebundle/' + idbundle)
      return this.httpclient.post("http://localhost:8080/profilebundle/" + idbundle, idprofile);
   }

   GetIdprofileofBundle(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/profilebundle/getidprofile/"+id);
   }

   DeleteProfilebundleById(id:any): Observable<any> {
      console.log("http://localhost:8080/profilebundle/"+id);
      return this.httpclient.delete("http://localhost:8080/profilebundle/"+id);
   }


   //#################### Bundleofferelementaire ####################

   //Marketing create

   getNature(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/offerelementaire/nature")
   }

   getLastbundle(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundle/lastbundle")
   }

   getDAofAllNatures(nature?:string[]): Observable<any>{
      console.log('http://localhost:8080//offerelementaire/getDAofnature')
      return this.httpclient.post("http://localhost:8080/offerelementaire/getDAofnature", nature);
   }

   getDAofNature(nature?:any): Observable<any>{
      console.log('http://localhost:8080//offerelementaire/getDAofnature/'+nature)
      return this.httpclient.get("http://localhost:8080/offerelementaire/getDAofnature/"+nature);
   }

   getUnitofAllNatures(nature?:string[]): Observable<any>{
      console.log('http://localhost:8080//offerelementaire/getUnitofnature')
      return this.httpclient.post("http://localhost:8080/offerelementaire/getUnitofnature", nature);
   }

   getUnitofNature(nature?:any): Observable<any>{
      console.log('http://localhost:8080//offerelementaire/getUnitofnature/'+nature)
      return this.httpclient.get("http://localhost:8080/offerelementaire/getUnitofnature/"+nature);
   }

   CreateBundleofferelementaire( idbundle?:any, DAquantity?: any): Observable<any> {
      console.log('url : http://localhost:8080​​/bundleOfferelementaire​/' + idbundle);
      console.log('DAquantity : ',JSON.stringify(DAquantity))
      return this.httpclient.post(`http://localhost:8080/bundleOfferelementaire/${idbundle}`, DAquantity);
   }

   //Marketing BundleOfferelementaire (offre modal)

   getBundleofferelementaireByIdbundle(idbundle?:any): Observable<any>{
      console.log('http://localhost:8080//bundleOfferelementaire/'+ idbundle)
      return this.httpclient.get(`http://localhost:8080//bundleOfferelementaire/${idbundle}`);
   }

   GetOneOfferelementaire(DA?:any): Observable<any>{
      console.log('http://localhost:8080//offerelementaire/getOne/'+ DA)
      return this.httpclient.get(`http://localhost:8080//offerelementaire/getOne/${DA}`);
   }

   UpdateBundleofferelementaireMk( idbundle?:any, DAquantity?: any): Observable<any> {
      console.log('url : http://localhost:8080​​/bundleOfferelementaire​/' + idbundle);
      console.log('DAquantity : ',JSON.stringify(DAquantity))
      return this.httpclient.put(`http://localhost:8080/bundleOfferelementaire/${idbundle}`, DAquantity);
   }

   DeleteBundleofferelementaireMk(idbundle?:any, DA?: any): Observable<any> {
      console.log('url : http://localhost:8080/bundleOfferelementaire/'+idbundle+'/'+DA);
      return this.httpclient.delete("http://localhost:8080/bundleOfferelementaire/"+idbundle+"/"+DA);
   } 

   //Technique BundleOfferelementaire

   getBundleofferelementaire(): Observable<any>{
      return this.httpclient.get("http://localhost:8080/bundleOfferelementaire")
   }

   UpdateBundleofferelementaire(DA: any,  offerId: any, data:BundleOfferelementaire): Observable<any> {
      console.log("url : http://localhost:8080/bundleOfferelementaire/"+data.idbundle+"/"+DA+"/"+offerId);
      return this.httpclient.put("http://localhost:8080/bundleOfferelementaire/"+data.idbundle+"/"+DA+"/"+offerId,data);
   }

   GetOneBundleofferelementaire(id:any): Observable<any> {
      return this.httpclient.get("http://localhost:8080/bundleOfferelementaire/"+id);
   }

   DeleteBundleofferelementaire(data:BundleOfferelementaire): Observable<any> {
      return this.httpclient.delete("http://localhost:8080/bundleOfferelementaire/"+data.idbundle+"/"+data.DA+"/"+data.offerId);
   }  


   //#################### Login ####################

   //Admin Login

   getLogin(): Observable<any>{
      return this.httpclient.get("http://localhost:8081/login");
   }
         
   CreateLogin(data:Login): Observable<any> {
      return this.httpclient.post("http://localhost:8081/login",data);
   }

   UpdateLogin(data:Login): Observable<any> {
      return this.httpclient.put("http://localhost:8081/login/"+data.trigramme,data);
   }

   getByTrigramme(data:Login): Observable<any> {
      console.log('url : http://localhost:8081/login/' +data.trigramme);
      return this.httpclient.get("http://localhost:8081/login/"+data.trigramme);
   }

   DeleteLogin(trigramme:any): Observable<any> {
      return this.httpclient.delete("http://localhost:8081/login/"+trigramme);
   }

   
   // Update Login ####################

   GetOneLoginById(trigramme?:any): Observable<any>{
      console.log('http://localhost:8081/login/'+trigramme);
      return this.httpclient.get("http://localhost:8081/login/"+trigramme);
   }


}
