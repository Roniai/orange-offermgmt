import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { freeApiService } from '../services/freeapi.service';
import { AuthService } from '../services/auth.service';
import { Offerelementaire} from '../classes/offerelementaire';
import { Unit } from '../classes/unit';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-technique-offerelementaire',
  templateUrl: './technique-offerelementaire.component.html',
  styleUrls: ['./technique-offerelementaire.component.css']
})
export class TechniqueOfferelementaireComponent implements OnInit {
  roleuser:any="";
  
  lstOfferelementaire :Offerelementaire[];
  offerelementaire:Offerelementaire= new Offerelementaire();
  isEdit:boolean=false;
  lstUnit :Unit[]=[];
  lstnature:[]=[];


  constructor( private _freeApiService: freeApiService,private authService: AuthService) { this.lstOfferelementaire=[]; }

  ngOnInit(): void {
    this.roleuser = this.authService.roleuserService;
    
    ///get
    this._freeApiService.getOfferelementaire().subscribe
    (
      data=> {
        this.lstOfferelementaire = data;
      }
    );
    ///get
    this._freeApiService.getUnit().subscribe
    (
      data=> {
        this.lstUnit = data;
      }
    );
    this._freeApiService. getNature().subscribe
    (
      data=> {
        this.lstnature = data;
      }
    );
  }

  
 onSubmit(form:NgForm):void{
  if(!this.isEdit){
  
    //Récupérer la valeur du input text nature
    var x = (<HTMLSelectElement>document.getElementById("nature"));
    var input = (<HTMLInputElement>document.getElementById("inputNature"));
      if(x.options[x.selectedIndex].value=="autre"){
        this.offerelementaire.nature = input.value;
      }

    console.log(this.offerelementaire);

    this._freeApiService.CreateOfferelementaire(this.offerelementaire).subscribe(
      resp=>{
        console.log(resp);

        this.closeModale();
        this.hideInputNature();

        Swal.fire({
          position: 'center',
          icon: 'success',
          text: 'La nouvelle offre élémentaire est créée avec succès',
          showConfirmButton: false,
          timer: 2500
        })
    
        },
      error1=>{
        Swal.fire({
          position: 'center',
          icon: 'error',
          text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
          showConfirmButton: false,
          timer: 2500
        })
    });

  }
  //PUt
    else{
      console.log(form.value);

      var x = (<HTMLSelectElement>document.getElementById("nature"));
      var input = (<HTMLInputElement>document.getElementById("inputNature"));
      if(x.options[x.selectedIndex].value=="autre"){
        this.offerelementaire.nature = input.value;
      }

      console.log(this.offerelementaire);

      this._freeApiService.UpdateOfferelementaire(this.offerelementaire).subscribe(
        resp=>{
          console.log(resp);
    
          this.closeModale();
          this.hideInputNature();

          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'La mise à jour de l\'offerelementaire "'+this.offerelementaire.DA+'" a été bien effectuée',
            showConfirmButton: false,
            timer: 2500
          }) 
        
          },
        error1=>{
          Swal.fire({
            position: 'center',
            icon: 'error',
            text: 'Une erreur s\'est produite. Veuillez réessayer à nouveau',
            showConfirmButton: false,
            timer: 2500
          })
      });
    }

  }

  add():void{
    this.isEdit=false;
    this.offerelementaire=new Offerelementaire;

    //Par défaut REST
    this.offerelementaire.typeInfoConso="REST";

    this.hideInputNature();
  }

  //Put(maka value fotsiny )
  edit(data:Offerelementaire):void{
    this.isEdit=true;
    this.offerelementaire=data;
    
    this.refreshUnit();

    this.hideInputNature();
    console.log(this.offerelementaire);
    
  }

  //delete
  delete(data:Offerelementaire):void{
      Swal.fire({
        title: 'Êtes-vous sûr?',
        text: "Vous ne pourrez plus revenir en arrière!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        cancelButtonColor: 'black',
        confirmButtonText: 'Oui, supprime-le!',
        confirmButtonColor: '#ff6600' 

      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'Offerelementaire "'+data.nature+'" a été supprimé définitivement',
            showConfirmButton: false,
            timer: 2500
          
          })
          this._freeApiService.DeleteOfferelementaire(data.DA).subscribe(resp=>{
              this.ngOnInit();
          });
        }
      })

  }

  changeSelectedNature(e:any){
    //Saisir une nouvelle nature hormis de la liste déroulante
    var input = (<HTMLInputElement>document.getElementById("inputNature"));
      if(e.target.value=="autre"){
        input.style.display='block';
        input.value="";
      }
      else{
        input.style.display='none';
      }
    
  }

  hideInputNature(){
    //Cacher inputNature
    var input = (<HTMLInputElement>document.getElementById("inputNature"));
    input.style.display='none';
    input.value="";
  }

  refreshUnit(){
    this.lstUnit=[];
    this._freeApiService.getUnit().subscribe
    (
      data=> {
        this.lstUnit = data;
      }
    );
  }

  onlyInteger(e:any){
    return e.charCode >= 48 && e.charCode <= 57;
  }
  
  closeModale(){
    //Fermer modale
    let x = document.getElementById('id01');
    if(x!=null){x.style.display='none'}
    this.ngOnInit();
  }

}


