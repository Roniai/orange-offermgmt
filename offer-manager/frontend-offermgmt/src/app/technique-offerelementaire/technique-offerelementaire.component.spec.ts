import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechniqueOfferelementaireComponent } from './technique-offerelementaire.component';

describe('TechniqueOfferelementaireComponent', () => {
  let component: TechniqueOfferelementaireComponent;
  let fixture: ComponentFixture<TechniqueOfferelementaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechniqueOfferelementaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechniqueOfferelementaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
