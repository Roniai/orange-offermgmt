package com.oma;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginApi {

    @Inject
    EntityManager em;

    //-------------------Admin level-------------------

    //Get list of all users login
    @GET
    public List<Login> getUserLogin(){
        return em.createQuery("SELECT l FROM Login l", Login.class)
                .getResultList();
    }


    //Get a user by idlogin
    @GET
    @Path("{trigramme}")
    public Login getByTrigramme(@PathParam("trigramme") String trigramme){
        List<Login> log = em.createQuery("SELECT l FROM Login l WHERE l.trigramme=:id", Login.class)
                .setParameter("id", trigramme)
                .getResultList();
        return log.get(0);
    }


    //Add new user login
    @POST
    @Transactional //use in CRUD operation
    public Login addUserLogin(Login login){
        em.createNativeQuery("INSERT INTO login(trigramme, depuser, roleuser, activeuser) VALUES(?,?,?,?)")
                .setParameter(1, login.trigramme)
                .setParameter(2, login.depuser)
                .setParameter(3, login.roleuser)
                .setParameter(4, login.activeuser)
                .executeUpdate();
        return login;
    }

    //Update the user login created
    @PUT
    @Path("{trigramme}")
    @Transactional
    public Login updateUserLogin(@PathParam("trigramme") String trigramme, Login login){
        em.createNativeQuery("UPDATE login SET trigramme=?, depuser=?, roleuser=?, activeuser=? WHERE trigramme=?")
                .setParameter(1, login.trigramme)
                .setParameter(2, login.depuser)
                .setParameter(3, login.roleuser)
                .setParameter(4, login.activeuser)
                .setParameter(5, trigramme)
                .executeUpdate();
        return login;
    }

    //Delete a user login
    @DELETE
    @Path("{trigramme}")
    @Transactional
    public int deleteUserLogin(@PathParam("trigramme") String trigramme){

        return em.createNativeQuery("DELETE FROM login WHERE trigramme=?")
                .setParameter(1, trigramme)
                .executeUpdate();
    }
}
