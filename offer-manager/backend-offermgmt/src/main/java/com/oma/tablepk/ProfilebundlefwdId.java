package com.oma.tablepk;

import java.io.Serializable;

public class ProfilebundlefwdId implements Serializable {
    public Integer idbundle;
    public String idprofile;

    public ProfilebundlefwdId() {
    }

    public ProfilebundlefwdId(Integer idbundle, String idprofile) {
        this.idbundle = idbundle;
        this.idprofile = idprofile;
    }
}
