package com.oma.tablepk;

import java.io.Serializable;

public class ProfileotherserviceId implements Serializable {
    public String idprofile;
    public Integer idotherservice;

    public ProfileotherserviceId() {
    }

    public ProfileotherserviceId(String idprofile, Integer idotherservice) {
        this.idprofile = idprofile;
        this.idotherservice = idotherservice;
    }
}
