package com.oma.tablepk;

import java.io.Serializable;

public class PublicdaId implements Serializable {
    public String idprofile;
    public Integer DA;

    public PublicdaId(){
    }

    public PublicdaId(String idprofile, Integer DA) {
        this.idprofile = idprofile;
        this.DA = DA;
    }
}
