package com.oma.api;

import com.oma.entity.Publicda;
import com.oma.repository.PublicdaRepository;
import io.quarkus.panache.common.Sort;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/publicda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PublicdaApi {

    @Inject
    PublicdaRepository publicdaRepository;

    //Get the list of publicda
    @GET
    public List<Publicda> listPublicda(){
        return Publicda.listAll();
    }

    @GET
    @Path("/groups1")
    public List<Publicda> getGroups1(){
        return Publicda.list("SELECT DISTINCT(p.groups1) FROM Publicda p WHERE p.groups1<>'' AND p.groups1<>NULL",
                Sort.by("p.groups1"));
    }
    @GET
    @Path("/groups2")
    public List<Publicda> getGroups2(){
        return Publicda.list("SELECT DISTINCT(p.groups2) FROM Publicda p WHERE p.groups2<>'' AND p.groups2<>NULL",
                Sort.by("p.groups2"));
    }
    @GET
    @Path("/groups3")
    public List<Publicda> getGroups3(){
        return Publicda.list("SELECT DISTINCT(p.groups3) FROM Publicda p WHERE p.groups3<>'' AND p.groups3<>NULL",
                Sort.by("p.groups3"));
    }

    //Add new publicda
    @POST
    @Transactional //use in CRUD operation
    public Response addPublicda(Publicda publicda){
        publicda.persist();
        return Response.status(Response.Status.CREATED).entity(publicda).build();
    }

    //Update the publicda created
    @PUT
    @Path("{idprofile}/{DA}")
    @Transactional
    public Response updatePublicda(
            @PathParam("idprofile") String idprofile,
            @PathParam("DA") Integer DA,
            Publicda publicda){
        Publicda pEntity = publicdaRepository.update(idprofile, DA, publicda);
        return Response.ok(pEntity).build();
    }
}
