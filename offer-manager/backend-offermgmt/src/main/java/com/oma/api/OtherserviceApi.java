package com.oma.api;

import com.oma.entity.Otherservice;
import com.oma.repository.OtherserviceRepository;
import io.quarkus.panache.common.Sort;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/otherservice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OtherserviceApi {

    @Inject
    OtherserviceRepository otherserviceRepository;

    @Inject
    EntityManager em;

    //Get the list of otherservice
    @GET
    public List<Otherservice> listOtherservice(){
        return em.createQuery("SELECT o FROM Otherservice o", Otherservice.class).getResultList();
    }

    /*Get Groups*/
    @GET
    @Path("/groups")
    public List<Otherservice> getGroups(){
        return Otherservice.list("SELECT DISTINCT(o.groups) FROM Otherservice o WHERE o.groups<>'' AND o.groups<>NULL",
                Sort.by("o.groups"));
    }
    @GET
    @Path("/groups1")
    public List<Otherservice> getGroups1(){
        return Otherservice.list("SELECT DISTINCT(o.groups1) FROM Otherservice o WHERE o.groups1<>'' AND o.groups1<>NULL",
                Sort.by("o.groups1"));
    }
    @GET
    @Path("/groups2")
    public List<Otherservice> getGroups2(){
        return Otherservice.list("SELECT DISTINCT(o.groups2) FROM Otherservice o WHERE o.groups2<>'' AND o.groups2<>NULL",
                Sort.by("o.groups2"));
    }
    @GET
    @Path("/groups3")
    public List<Otherservice> getGroups3(){
        return Otherservice.list("SELECT DISTINCT(o.groups3) FROM Otherservice o WHERE o.groups3<>'' AND o.groups3<>NULL",
                Sort.by("o.groups3"));
    }

    //Add new otherservice
    //SQLException accept to use order when we use `order`
    @POST
    @Transactional //use in CRUD operation
    public Otherservice addOtherservice(Otherservice o){
        em.createNativeQuery("" +
            "INSERT INTO otherservice(idotherservice,refill,nom,description,prix,taxe,isActive,validite," +
                "groups,grouprank,groups1,groupsrank1,groups2,groupsrank2,groups3,groupsrank3," +
                "`order`,commentaire,groupsrank) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                .setParameter(1, null)
                .setParameter(2, o.refill)
                .setParameter(3, o.nom)
                .setParameter(4, o.description)
                .setParameter(5, o.prix)
                .setParameter(6, o.taxe)
                .setParameter(7, o.isActive)
                .setParameter(8, o.validite)
                .setParameter(9, o.groups)
                .setParameter(10, o.grouprank)
                .setParameter(11, o.groups1)
                .setParameter(12, o.groupsrank1)
                .setParameter(13, o.groups2)
                .setParameter(14, o.groupsrank2)
                .setParameter(15, o.groups3)
                .setParameter(16, o.groupsrank3)
                .setParameter(17, o.order)
                .setParameter(18, o.commentaire)
                .setParameter(19, o.groupsrank)
                .executeUpdate();

        //Get the last record of otherservice
        List<Otherservice> listOthers = Otherservice.list("SELECT o FROM Otherservice o ", Sort.descending("o.idotherservice"));
        return listOthers.get(0);
    }

    //Update the otherservice created
    @PUT
    @Path("{idotherservice}")
    @Transactional //use in CRUD operation
    public Otherservice updateOtherservice(@PathParam("idotherservice") Integer idotherservice, Otherservice o){
        em.createNativeQuery("" +
                "UPDATE otherservice SET refill=?,nom=?,description=?,prix=?,taxe=?,isActive=?,validite=?," +
                "groups=?,grouprank=?,groups1=?,groupsrank1=?,groups2=?,groupsrank2=?,groups3=?,groupsrank3=?," +
                "`order`=?,commentaire=?,groupsrank=? WHERE idotherservice=?")
                .setParameter(1, o.refill)
                .setParameter(2, o.nom)
                .setParameter(3, o.description)
                .setParameter(4, o.prix)
                .setParameter(5, o.taxe)
                .setParameter(6, o.isActive)
                .setParameter(7, o.validite)
                .setParameter(8, o.groups)
                .setParameter(9, o.grouprank)
                .setParameter(10, o.groups1)
                .setParameter(11, o.groupsrank1)
                .setParameter(12, o.groups2)
                .setParameter(13, o.groupsrank2)
                .setParameter(14, o.groups3)
                .setParameter(15, o.groupsrank3)
                .setParameter(16, o.order)
                .setParameter(17, o.commentaire)
                .setParameter(18, o.groupsrank)
                .setParameter(19, idotherservice)
                .executeUpdate();
        return o;
    }
}
