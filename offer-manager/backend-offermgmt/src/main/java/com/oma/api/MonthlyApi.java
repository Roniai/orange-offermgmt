package com.oma.api;

import com.oma.entity.Monthly;
import com.oma.repository.MonthlyRepository;
import io.quarkus.panache.common.Sort;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/monthly")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MonthlyApi {

    @Inject
    MonthlyRepository monthlyRepository;

    //Get the list of monthly
    @GET
    public List<Monthly> listMonthly(){
        return Monthly.listAll();
    }

    /*Get Groups*/
    @GET
    @Path("/groups")
    public List<Monthly> getGroups(){
        return Monthly.list("SELECT DISTINCT(m.groups) FROM Monthly m WHERE m.groups<>'' AND m.groups<>NULL",
                Sort.by("m.groups"));
    }
    @GET
    @Path("/groups1")
    public List<Monthly> getGroups1(){
        return Monthly.list("SELECT DISTINCT(m.groups1) FROM Monthly m WHERE m.groups1<>'' AND m.groups1<>NULL",
                Sort.by("m.groups1"));
    }
    @GET
    @Path("/groups2")
    public List<Monthly> getGroups2(){
        return Monthly.list("SELECT DISTINCT(m.groups2) FROM Monthly m WHERE m.groups2<>'' AND m.groups2<>NULL",
                Sort.by("m.groups2"));
    }
    @GET
    @Path("/groups3")
    public List<Monthly> getGroups3(){
        return Monthly.list("SELECT DISTINCT(m.groups3) FROM Monthly m WHERE m.groups3<>'' AND m.groups3<>NULL",
                Sort.by("m.groups3"));
    }

    //Add new monthly
    @POST
    @Transactional //use in CRUD operation
    public Response addMonthly(Monthly monthly){
        monthly.persist();
        return Response.status(Response.Status.CREATED).entity(monthly).build();
    }

    //Update the monthly created
    @PUT
    @Path("{id}")
    @Transactional
    public Response updateMonthly(
            @PathParam("id") Integer id, Monthly monthly){
        Monthly mEntity = monthlyRepository.update(id, monthly);
        return Response.ok(mEntity).build();
    }
}
