package com.oma.api;

import com.oma.entity.Profilebundlefwd;
import com.oma.tablepk.ProfilebundlefwdId;
import io.quarkus.panache.common.Parameters;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/profilebundlefwd")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfilebundlefwdApi {

    @Inject
    EntityManager em;

    //Get all idprofile of a idbundle
    @GET
    @Path("/getidprofile/{idbundle}")
    public List<Profilebundlefwd> getIdprofile(@PathParam("idbundle") Integer idbundle){
        return Profilebundlefwd.find("SELECT p.idprofile FROM Profilebundlefwd p WHERE p.idbundle= :idbundle",
                Parameters.with("idbundle", idbundle)).list();
    }

    //Add new profilebundlefwd
    @POST
    @Path("{idbundle}")
    @Transactional //use in CRUD operation
    public String[] addProfilebundlefwd(@PathParam("idbundle") Integer idbundle,
                                     String[] listIdprofile){
        for(String idprofile:listIdprofile){
            em.createNativeQuery("INSERT INTO profilebundlefwd VALUES(?,?)")
                .setParameter(1, idprofile)
                .setParameter(2, idbundle)
                .executeUpdate();
        }
        return listIdprofile;
    }

    //Delete a profilebundlefwd
    @DELETE
    @Path("{idbundle}/{idprofile}")
    @Transactional
    public boolean deleteProfilefwd(
            @PathParam("idprofile") String idprofile,
            @PathParam("idbundle") Integer idbundle){
        return Profilebundlefwd.deleteById(new ProfilebundlefwdId(idbundle, idprofile));
    }

}
