package com.oma.api;

import com.oma.entity.Bundle;
import com.oma.entity.Bundle_Mk;
import com.oma.repository.BundleMkRepository;
import com.oma.repository.BundleRepository;
import io.quarkus.panache.common.Sort;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/bundle")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BundleApi {

    @Inject
    BundleRepository bundleRepository;

    @Inject
    BundleMkRepository bundleMkRepository;

    @Inject
    EntityManager em;

    //-------------------Marketing level-------------------

    //Get the list of bundle
    @GET
    @Path("/mk")
    public List<Bundle_Mk> listBundle_Mk(){
        return Bundle_Mk.listAll();
    }

    //Get the last record of bundle
    @GET
    @Path("/lastbundle")
    @Transactional //use in CRUD operation
    public Bundle getLastbundle(){

        List<Bundle> b = Bundle.list("SELECT b FROM Bundle b ", Sort.descending("b.idbundle"));
        return b.get(0);
    }

    /*Get Groups*/
    @GET
    @Path("/groups")
    public List<Bundle> getGroups(){
        return Bundle.list("SELECT DISTINCT(b.groups) FROM Bundle b WHERE b.groups<>'' AND b.groups<>NULL",
                Sort.by("b.groups"));
    }
    @GET
    @Path("/groups1")
    public List<Bundle> getGroups1(){
        return Bundle.list("SELECT DISTINCT(b.groups1) FROM Bundle b WHERE b.groups1<>'' AND b.groups1<>NULL",
                Sort.by("b.groups1"));
    }
    @GET
    @Path("/groups2")
    public List<Bundle> getGroups2(){
        return Bundle.list("SELECT DISTINCT(b.groups2) FROM Bundle b WHERE b.groups2<>'' AND b.groups2<>NULL",
                Sort.by("b.groups2"));
    }
    @GET
    @Path("/groups3")
    public List<Bundle> getGroups3(){
        return Bundle.list("SELECT DISTINCT(b.groups3) FROM Bundle b WHERE b.groups3<>'' AND b.groups3<>NULL",
                Sort.by("b.groups3"));
    }
    @GET
    @Path("/groups4")
    public List<Bundle> getGroups4(){
        return Bundle.list("SELECT DISTINCT(b.groups4) FROM Bundle b WHERE b.groups4<>'' AND b.groups4<>NULL",
                Sort.by("b.groups4"));
    }
    @GET
    @Path("/groups5")
    public List<Bundle> getGroups5(){
        return Bundle.list("SELECT DISTINCT(b.groups5) FROM Bundle b WHERE b.groups5<>'' AND b.groups5<>NULL",
                Sort.by("b.groups5"));
    }

    //Add new bundle
    @POST
    @Path("/mk")
    @Transactional
    public Response addBundle(Bundle_Mk bundle){
        bundle.persist();
        return Response.status(Response.Status.CREATED).entity(bundle).build();
    }

    //Update the bundleMk created
    @PUT
    @Path("/mk/{idbundle}")
    @Transactional
    public Response updateBundle_Mk(
            @PathParam("idbundle") Integer idbundle, Bundle_Mk bundle){
        Bundle_Mk bEntity = bundleMkRepository.update(idbundle, bundle);
        return Response.ok(bEntity).build();
    }



    //-------------------Config level-------------------

    //Get the list of bundle (All of table)
    @GET
    public List<Bundle> listBundle(){
        return Bundle.listAll();
    }

    //Update the bundle created
    @PUT
    @Path("{idbundle}")
    @Transactional
    public Response updateBundle(
            @PathParam("idbundle") Integer idbundle, Bundle bundle){
        Bundle bEntity = bundleRepository.update(idbundle, bundle);
        return Response.ok(bEntity).build();
    }

    //Update the bundle created
    @PUT
    @Path("/mktc/{idbundle}")
    @Transactional
    public Response updateBundleMkTc(
            @PathParam("idbundle") Integer idbundle, Bundle bundle){
        Bundle bEntity = bundleRepository.updateMkTc(idbundle, bundle);
        return Response.ok(bEntity).build();
    }
}
