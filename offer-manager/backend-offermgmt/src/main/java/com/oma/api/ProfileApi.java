package com.oma.api;

import com.oma.entity.Profile;
import com.oma.repository.ProfileRepository;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/profile")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileApi {

    @Inject
    ProfileRepository profileRepository;

    //Get the list of profile
    @GET
    public List<Profile> listProfile(){
        return Profile.listAll();
    }

    //Get the list of idprofile
    @GET
    @Path("/idprofile")
    public List<Profile> listId(){
        return profileRepository.listIdProfile();
    }

    //Add new profile
    @POST
    @Transactional //use in CRUD operation
    public Response addProfile(Profile profile){
        profile.persist();
        return Response.status(Response.Status.CREATED).entity(profile).build();
    }

    //Update the profile created
    @PUT
    @Path("{idprofile}")
    @Transactional
    public Response updateProfile(
            @PathParam("idprofile") String idprofile, Profile profile){
        Profile pEntity = profileRepository.update(idprofile, profile);
        return Response.ok(pEntity).build();
    }

    //Delete a profile
    @DELETE
    @Path("{idprofile}")
    @Transactional
    public boolean deleteProfile(
            @PathParam("idprofile") String idprofile){
        return Profile.deleteById(idprofile);
    }
}
