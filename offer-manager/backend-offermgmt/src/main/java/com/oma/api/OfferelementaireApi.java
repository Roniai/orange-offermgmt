package com.oma.api;

import com.oma.entity.Offerelementaire;
import com.oma.repository.OfferelementaireRepository;
import io.quarkus.panache.common.Parameters;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/offerelementaire")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OfferelementaireApi {

    @Inject
    OfferelementaireRepository offerelementaireRepository;

    @Inject
    EntityManager em;

    //Get the list of offerelementaire
    @GET
    public List<Offerelementaire> listOfferelementaire(){
        return Offerelementaire.listAll();
    }

    //Select distinct nature+unit of Offerelementaire
    @GET
    @Path("/nature")
    public List<Offerelementaire> listAllNature() {
        return offerelementaireRepository.listNature();
    }

    //Find the DA of all natures in parameters
    //We use POST 'cause request with GET/HEAD method cannot have body.
    @POST
    @Path("/getDAofnature")
    public List<Integer> getDAofThoseNatures(String[] listNatures){
        List<Integer> listDA = new ArrayList<>();
        for(String nature : listNatures){
            List<Offerelementaire> listOfferes = offerelementaireRepository.getThisNature(nature);
            //We recuperate the first result
            Offerelementaire result = listOfferes.get(0);
            listDA.add(result.getDA());
        }
        return listDA;
    }

    @GET
    @Path("/getDAofnature/{nature}")
    public Integer getDAofThisNature(@PathParam("nature") String nature){
        List<Offerelementaire> listOfferes = offerelementaireRepository.getThisNature(nature);
        //We recuperate the first result
        Offerelementaire result = listOfferes.get(0);
        return result.getDA();
    }

    //Find the Unit of all natures in parameters
    @POST
    @Path("/getUnitofnature")
    public List<Integer> getUnitofThoseNatures(String[] listNatures){
        List<Integer> listUnit = new ArrayList<>();
        for(String nature : listNatures){
            List<Offerelementaire> listOfferes = offerelementaireRepository.getThisNature(nature);
            //We recuperate the first result
            Offerelementaire result = listOfferes.get(0);
            listUnit.add(result.getUnit());
        }
        return listUnit;
    }

    @GET
    @Path("/getUnitofnature/{nature}")
    public Integer getUnitofThisNature(@PathParam("nature") String nature){
        List<Offerelementaire> listOfferes = offerelementaireRepository.getThisNature(nature);
        //We recuperate the first result
        Offerelementaire result = listOfferes.get(0);
        return result.getUnit();
    }

    //Get a offerelementaire by DA
    @GET
    @Path("/getOne/{DA}")
    public Offerelementaire getNatureOfDA(@PathParam("DA") Integer DA){
        List<Offerelementaire> listOfferes = offerelementaireRepository.getOneOfferelementaire(DA);
        return listOfferes.get(0);
    }

    //Add new offerelementaire
    @POST
    @Transactional //use in CRUD operation
    public Response addOfferelementaire(Offerelementaire offerelementaire){
        offerelementaire.persist();
        return Response.status(Response.Status.CREATED).entity(offerelementaire).build();
    }

    //Update the offerelementaire created
    @PUT
    @Path("{DA}")
    @Transactional
    public Response updateOfferelementaire(
            @PathParam("DA") Integer DA, Offerelementaire offerelementaire){
        Offerelementaire oEntity = offerelementaireRepository.update(DA, offerelementaire);
        return Response.ok(oEntity).build();
    }

    //Delete a offerelementaire
    @DELETE
    @Path("{DA}")
    @Transactional
    public boolean deleteProfile(
            @PathParam("DA") Integer DA){
        return Offerelementaire.deleteById(DA);
    }
}
