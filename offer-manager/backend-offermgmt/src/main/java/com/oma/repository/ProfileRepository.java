package com.oma.repository;

import com.oma.entity.Offerelementaire;
import com.oma.entity.Profile;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class ProfileRepository {
    public Profile update(String idprofile, Profile profile){

        Profile profileUpdated = Profile.findById(idprofile);

        if (profileUpdated == null) {
            throw new WebApplicationException("The idprofile : " + idprofile + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the idprofile
        profileUpdated.setTitle(profile.getTitle());
        profileUpdated.setParent(profile.getParent());
        profileUpdated.setCommunityid(profile.getCommunityid());
        profileUpdated.setGroupeid(profile.getGroupeid());
        profileUpdated.setServiceclass(profile.getServiceclass());
        profileUpdated.setLcg(profile.getLcg());
        profileUpdated.setLcgref(profile.getLcgref());
        profileUpdated.setLcn(profile.getLcn());
        profileUpdated.setLcnref(profile.getLcnref());
        profileUpdated.setLcint(profile.getLcint());
        profileUpdated.setLcintref(profile.getLcintref());
        profileUpdated.setLcr(profile.getLcr());
        profileUpdated.setLcrref(profile.getLcrref());
        profileUpdated.setUcg(profile.getUcg());
        profileUpdated.setUcn(profile.getUcn());
        profileUpdated.setUcint(profile.getUcint());
        profileUpdated.setUcr(profile.getUcr());

        return profileUpdated;
    }

    public List<Profile> listIdProfile() {
        return Profile.list("SELECT p.idprofile FROM Profile p",
                Sort.by("p.idprofile"));
    }
}
