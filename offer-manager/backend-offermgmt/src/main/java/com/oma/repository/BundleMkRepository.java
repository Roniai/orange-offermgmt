package com.oma.repository;

import com.oma.entity.Bundle_Mk;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class BundleMkRepository {
    public Bundle_Mk update(Integer id, Bundle_Mk bundle){

        Bundle_Mk bundleUpdated = Bundle_Mk.findById(id);

        if (bundleUpdated == null) {
            throw new WebApplicationException("The bundle : " + id + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the id
        bundleUpdated.setNom(bundle.getNom());
        bundleUpdated.setIsBuyableForOther(bundle.getIsBuyableForOther());
        bundleUpdated.setIsautorenewable(bundle.getIsautorenewable());
        bundleUpdated.setIsbuylimited(bundle.getIsbuylimited());
        bundleUpdated.setBuymaximum(bundle.getBuymaximum());
        bundleUpdated.setBuyfrequency(bundle.getBuyfrequency());
        bundleUpdated.setDescription(bundle.getDescription());
        bundleUpdated.setPrix(bundle.getPrix());
        bundleUpdated.setTaxe(bundle.getTaxe());
        bundleUpdated.setValidite(bundle.getValidite());
        bundleUpdated.setIsBuyableByOM(bundle.getIsBuyableByOM());
        bundleUpdated.setIsBuyableWithLanyCredit(bundle.getIsBuyableWithLanyCredit());
        bundleUpdated.setGroups(bundle.getGroups());
        bundleUpdated.setGrouprank(bundle.getGrouprank());
        bundleUpdated.setGroups1(bundle.getGroups1());
        bundleUpdated.setGroupsrank1(bundle.getGroupsrank1());
        bundleUpdated.setGroups2(bundle.getGroups2());
        bundleUpdated.setGroupsrank2(bundle.getGroupsrank2());
        bundleUpdated.setGroups3(bundle.getGroups3());
        bundleUpdated.setGroupsrank3(bundle.getGroupsrank3());
        bundleUpdated.setGroups4(bundle.getGroups4());
        bundleUpdated.setGroupsrank4(bundle.getGroupsrank4());
        bundleUpdated.setGroups5(bundle.getGroups5());
        bundleUpdated.setGroupsrank5(bundle.getGroupsrank5());
        bundleUpdated.setGeneralrank(bundle.getGeneralrank());
        bundleUpdated.setCommentaire(bundle.getCommentaire());

        return bundleUpdated;
    }
}
