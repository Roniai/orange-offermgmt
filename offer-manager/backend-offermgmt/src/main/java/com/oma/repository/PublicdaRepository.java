package com.oma.repository;

import com.oma.entity.Publicda;
import com.oma.tablepk.PublicdaId;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class PublicdaRepository {
    public Publicda update(
            String idprofile,
            Integer DA,
            Publicda publicda){

        Publicda publicdaUpdated = Publicda.findById(new PublicdaId(idprofile,DA));

        if (publicdaUpdated == null) {
            throw new WebApplicationException("The idprofile : " + idprofile + "and DA :" + DA + " don't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the id(idprofile+DA)
        publicdaUpdated.setNom(publicda.getNom());
        publicdaUpdated.setIsroaming(publicda.getIsroaming());
        publicdaUpdated.setIstoexcludefrominfoconso(publicda.getIstoexcludefrominfoconso());
        publicdaUpdated.setIsmixed(publicda.getIsmixed());
        publicdaUpdated.setIsActive(publicda.getIsActive());
        publicdaUpdated.setTarifsmsnat(publicda.getTarifsmsnat());
        publicdaUpdated.setTarifappelnat(publicda.getTarifappelnat());
        publicdaUpdated.setTarifdata(publicda.getTarifdata());
        publicdaUpdated.setTarifappelfamille(publicda.getTarifappelfamille());
        publicdaUpdated.setTarifappelorange(publicda.getTarifappelorange());
        publicdaUpdated.setTarifappelinternational(publicda.getTarifappelinternational());
        publicdaUpdated.setTarifsmsfamille(publicda.getTarifsmsfamille());
        publicdaUpdated.setTarifsmsorange(publicda.getTarifsmsorange());
        publicdaUpdated.setTarifsmsinternational(publicda.getTarifsmsinternational());
        publicdaUpdated.setGroups1(publicda.getGroups1());
        publicdaUpdated.setGroupsrank1(publicda.getGroupsrank1());
        publicdaUpdated.setGroups2(publicda.getGroups2());
        publicdaUpdated.setGroupsrank2(publicda.getGroupsrank2());
        publicdaUpdated.setGroups3(publicda.getGroups3());
        publicdaUpdated.setGroupsrank3(publicda.getGroupsrank3());

        return publicdaUpdated;
    }
}
