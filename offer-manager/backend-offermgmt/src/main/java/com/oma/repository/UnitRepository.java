package com.oma.repository;

import com.oma.entity.Unit;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class UnitRepository {
    public Unit update(Integer id, Unit unit){

        Unit unitUpdated = Unit.findById(id);

        if (unitUpdated == null) {
            throw new WebApplicationException("The unit : " + id + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the id
        unitUpdated.setDescription(unit.getDescription());
        unitUpdated.setSystemratio(unit.getSystemratio());
        return unitUpdated;
    }
}
