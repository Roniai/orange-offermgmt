package com.oma.repository;

import com.oma.entity.Monthly;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class MonthlyRepository {
    public Monthly update(Integer id, Monthly monthly){

        Monthly monthlyUpdated = Monthly.findById(id);

        if (monthlyUpdated == null) {
            throw new WebApplicationException("The id : " + id + "doesn't exist.", Response.Status.NOT_FOUND);
        }

        //We cannot change the id
        monthlyUpdated.setNom(monthly.getNom());
        monthlyUpdated.setDa(monthly.getDa());
        monthlyUpdated.setOfferID(monthly.getOfferID());
        monthlyUpdated.setIsActive(monthly.getIsActive());
        monthlyUpdated.setGroups(monthly.getGroups());
        monthlyUpdated.setGroupsrank(monthly.getGroupsrank());
        monthlyUpdated.setGroups1(monthly.getGroups1());
        monthlyUpdated.setGroupsrank1(monthly.getGroupsrank1());
        monthlyUpdated.setGroups2(monthly.getGroups2());
        monthlyUpdated.setGroupsrank2(monthly.getGroupsrank2());
        monthlyUpdated.setGroups3(monthly.getGroups3());
        monthlyUpdated.setGroupsrank3(monthly.getGroupsrank3());

        return monthlyUpdated;
    }
}
