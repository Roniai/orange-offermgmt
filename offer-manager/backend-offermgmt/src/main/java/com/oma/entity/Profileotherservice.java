package com.oma.entity;

import com.oma.tablepk.ProfileotherserviceId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
//The IdClass annotation maps multiple fields to the table PK (PrimaryKey).
@IdClass(ProfileotherserviceId.class)
public class Profileotherservice extends PanacheEntityBase {
    @Id
    public String idprofile;
    @Id
    public Integer idotherservice;
}
