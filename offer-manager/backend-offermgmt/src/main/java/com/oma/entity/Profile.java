package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Profile extends PanacheEntityBase {
    @Id
    public String idprofile;

    public String title;
    public String parent;
    public String communityid;
    public String groupeid;
    public String serviceclass;
    public Integer lcg;
    public Integer lcgref;
    public Integer lcn;
    public Integer lcnref;
    public Integer lcint;
    public Integer lcintref;
    public Integer lcr;
    public Integer lcrref;
    public Integer ucg;
    public Integer ucn;
    public Integer ucint;
    public Integer ucr;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getCommunityid() {
        return communityid;
    }

    public void setCommunityid(String communityid) {
        this.communityid = communityid;
    }

    public String getGroupeid() {
        return groupeid;
    }

    public void setGroupeid(String groupeid) {
        this.groupeid = groupeid;
    }

    public String getServiceclass() {
        return serviceclass;
    }

    public void setServiceclass(String serviceclass) {
        this.serviceclass = serviceclass;
    }

    public Integer getLcg() {
        return lcg;
    }

    public void setLcg(Integer lcg) {
        this.lcg = lcg;
    }

    public Integer getLcgref() {
        return lcgref;
    }

    public void setLcgref(Integer lcgref) {
        this.lcgref = lcgref;
    }

    public Integer getLcn() {
        return lcn;
    }

    public void setLcn(Integer lcn) {
        this.lcn = lcn;
    }

    public Integer getLcnref() {
        return lcnref;
    }

    public void setLcnref(Integer lcnref) {
        this.lcnref = lcnref;
    }

    public Integer getLcint() {
        return lcint;
    }

    public void setLcint(Integer lcint) {
        this.lcint = lcint;
    }

    public Integer getLcintref() {
        return lcintref;
    }

    public void setLcintref(Integer lcintref) {
        this.lcintref = lcintref;
    }

    public Integer getLcr() {
        return lcr;
    }

    public void setLcr(Integer lcr) {
        this.lcr = lcr;
    }

    public Integer getLcrref() {
        return lcrref;
    }

    public void setLcrref(Integer lcrref) {
        this.lcrref = lcrref;
    }

    public Integer getUcg() {
        return ucg;
    }

    public void setUcg(Integer ucg) {
        this.ucg = ucg;
    }

    public Integer getUcn() {
        return ucn;
    }

    public void setUcn(Integer ucn) {
        this.ucn = ucn;
    }

    public Integer getUcint() {
        return ucint;
    }

    public void setUcint(Integer ucint) {
        this.ucint = ucint;
    }

    public Integer getUcr() {
        return ucr;
    }

    public void setUcr(Integer ucr) {
        this.ucr = ucr;
    }
}
