package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
public class Bundle extends PanacheEntityBase {
    @Id
    public Integer idbundle;

    //Marketing level
    public String nom;
    public Integer isBuyableForOther;
    public Integer isautorenewable;
    public Integer 	isbuylimited;
    public Integer buymaximum;
    public String buyfrequency;
    public String description;
    public float prix;
    public String taxe;
    public String validite;
    public Integer isBuyableByOM;
    public Integer isBuyableWithLanyCredit;
    public String groups;
    public Integer grouprank;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;
    public String groups4;
    public Integer groupsrank4;
    public String groups5;
    public Integer groupsrank5;
    public Integer generalrank;
    public String commentaire;

    //Config level
    public Integer isroaming;
    public Integer isActive;
    public String refill;
    public String refillinjection;
    public String refillfwd;
    public String refillapi;
    public String refilllms;
    public String refillmypos;
    public Integer chargingindicator;
    public Integer chargingindicatorom;
    public Integer pamclass;
    public Integer pamservice;
    public Integer pamscheduled;
    public Integer accumulatorid;
    public String refillom;
    public String refilllanycredit;



    /*Marketing*/

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getIsBuyableForOther() {
        return isBuyableForOther;
    }

    public void setIsBuyableForOther(Integer isBuyableForOther) {
        this.isBuyableForOther = isBuyableForOther;
    }

    public Integer getIsautorenewable() {
        return isautorenewable;
    }

    public void setIsautorenewable(Integer isautorenewable) {
        this.isautorenewable = isautorenewable;
    }

    public Integer getIsbuylimited() {
        return isbuylimited;
    }

    public void setIsbuylimited(Integer isbuylimited) {
        this.isbuylimited = isbuylimited;
    }

    public Integer getBuymaximum() {
        return buymaximum;
    }

    public void setBuymaximum(Integer buymaximum) {
        this.buymaximum = buymaximum;
    }

    public String getBuyfrequency() {
        return buyfrequency;
    }

    public void setBuyfrequency(String buyfrequency) {
        this.buyfrequency = buyfrequency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getTaxe() {
        return taxe;
    }

    public void setTaxe(String taxe) {
        this.taxe = taxe;
    }

    public String getValidite() {
        return validite;
    }

    public void setValidite(String validite) {
        this.validite = validite;
    }

    public Integer getIsBuyableByOM() {
        return isBuyableByOM;
    }

    public void setIsBuyableByOM(Integer isBuyableByOM) {
        this.isBuyableByOM = isBuyableByOM;
    }

    public Integer getIsBuyableWithLanyCredit() {
        return isBuyableWithLanyCredit;
    }

    public void setIsBuyableWithLanyCredit(Integer isBuyableWithLanyCredit) {
        this.isBuyableWithLanyCredit = isBuyableWithLanyCredit;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public Integer getGrouprank() {
        return grouprank;
    }

    public void setGrouprank(Integer grouprank) {
        this.grouprank = grouprank;
    }

    public String getGroups1() {
        return groups1;
    }

    public void setGroups1(String groups1) {
        this.groups1 = groups1;
    }

    public Integer getGroupsrank1() {
        return groupsrank1;
    }

    public void setGroupsrank1(Integer groupsrank1) {
        this.groupsrank1 = groupsrank1;
    }

    public String getGroups2() {
        return groups2;
    }

    public void setGroups2(String groups2) {
        this.groups2 = groups2;
    }

    public Integer getGroupsrank2() {
        return groupsrank2;
    }

    public void setGroupsrank2(Integer groupsrank2) {
        this.groupsrank2 = groupsrank2;
    }

    public String getGroups3() {
        return groups3;
    }

    public void setGroups3(String groups3) {
        this.groups3 = groups3;
    }

    public Integer getGroupsrank3() {
        return groupsrank3;
    }

    public void setGroupsrank3(Integer groupsrank3) {
        this.groupsrank3 = groupsrank3;
    }

    public String getGroups4() {
        return groups4;
    }

    public void setGroups4(String groups4) {
        this.groups4 = groups4;
    }

    public Integer getGroupsrank4() {
        return groupsrank4;
    }

    public void setGroupsrank4(Integer groupsrank4) {
        this.groupsrank4 = groupsrank4;
    }

    public String getGroups5() {
        return groups5;
    }

    public void setGroups5(String groups5) {
        this.groups5 = groups5;
    }

    public Integer getGroupsrank5() {
        return groupsrank5;
    }

    public void setGroupsrank5(Integer groupsrank5) {
        this.groupsrank5 = groupsrank5;
    }

    public Integer getGeneralrank() {
        return generalrank;
    }

    public void setGeneralrank(Integer generalrank) {
        this.generalrank = generalrank;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }




    /*Technique*/
    public Integer getIdbundle() {
        return idbundle;
    }

    public Integer getIsroaming() {
        return isroaming;
    }

    public void setIsroaming(Integer isroaming) {
        this.isroaming = isroaming;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getRefill() {
        return refill;
    }

    public void setRefill(String refill) {
        this.refill = refill;
    }

    public String getRefillinjection() {
        return refillinjection;
    }

    public void setRefillinjection(String refillinjection) {
        this.refillinjection = refillinjection;
    }

    public String getRefillfwd() {
        return refillfwd;
    }

    public void setRefillfwd(String refillfwd) {
        this.refillfwd = refillfwd;
    }

    public String getRefillapi() {
        return refillapi;
    }

    public void setRefillapi(String refillapi) {
        this.refillapi = refillapi;
    }

    public String getRefilllms() {
        return refilllms;
    }

    public void setRefilllms(String refilllms) {
        this.refilllms = refilllms;
    }

    public String getRefillmypos() {
        return refillmypos;
    }

    public void setRefillmypos(String refillmypos) {
        this.refillmypos = refillmypos;
    }

    public Integer getChargingindicator() {
        return chargingindicator;
    }

    public void setChargingindicator(Integer chargingindicator) {
        this.chargingindicator = chargingindicator;
    }

    public Integer getChargingindicatorom() {
        return chargingindicatorom;
    }

    public void setChargingindicatorom(Integer chargingindicatorom) {
        this.chargingindicatorom = chargingindicatorom;
    }

    public Integer getPamclass() {
        return pamclass;
    }

    public void setPamclass(Integer pamclass) {
        this.pamclass = pamclass;
    }

    public Integer getPamservice() {
        return pamservice;
    }

    public void setPamservice(Integer pamservice) {
        this.pamservice = pamservice;
    }

    public Integer getPamscheduled() {
        return pamscheduled;
    }

    public void setPamscheduled(Integer pamscheduled) {
        this.pamscheduled = pamscheduled;
    }

    public Integer getAccumulatorid() {
        return accumulatorid;
    }

    public void setAccumulatorid(Integer accumulatorid) {
        this.accumulatorid = accumulatorid;
    }

    public String getRefillom() {
        return refillom;
    }

    public void setRefillom(String refillom) {
        this.refillom = refillom;
    }

    public String getRefilllanycredit() {
        return refilllanycredit;
    }

    public void setRefilllanycredit(String refilllanycredit) {
        this.refilllanycredit = refilllanycredit;
    }
}
