package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Unit extends PanacheEntityBase {
    @Id
    public Integer unit;
    public String description;
    public float systemratio;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getSystemratio() {
        return systemratio;
    }

    public void setSystemratio(float systemratio) {
        this.systemratio = systemratio;
    }
}
