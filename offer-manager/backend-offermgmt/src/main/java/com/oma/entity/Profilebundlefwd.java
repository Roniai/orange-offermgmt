package com.oma.entity;

import com.oma.tablepk.ProfilebundlefwdId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
//The IdClass annotation maps multiple fields to the table PK (PrimaryKey).
@IdClass(ProfilebundlefwdId.class)
public class Profilebundlefwd extends PanacheEntityBase {
    @Id
    public Integer idbundle;
    @Id
    public String idprofile;
}
