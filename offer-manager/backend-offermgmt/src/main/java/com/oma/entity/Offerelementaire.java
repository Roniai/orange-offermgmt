package com.oma.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Offerelementaire extends PanacheEntityBase{
    @Id
    public Integer DA;

    public String typeInfoConso;
    public String nature;
    public String typeOffer;
    public Integer unit;

    public Integer getDA() {
        return DA;
    }

    public String getTypeInfoConso() {
        return typeInfoConso;
    }

    public void setTypeInfoConso(String typeInfoConso) {
        this.typeInfoConso = typeInfoConso;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getTypeOffer() {
        return typeOffer;
    }

    public void setTypeOffer(String typeOffer) {
        this.typeOffer = typeOffer;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }
}
