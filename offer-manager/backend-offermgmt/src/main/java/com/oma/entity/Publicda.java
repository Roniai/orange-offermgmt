package com.oma.entity;

import com.oma.tablepk.PublicdaId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
//The IdClass annotation maps multiple fields to the table PK (PrimaryKey).
@IdClass(PublicdaId.class)
public class Publicda extends PanacheEntityBase {
    @Id
    public String idprofile;
    @Id
    public Integer DA;

    public String nom;
    public Integer isroaming;
    public Integer istoexcludefrominfoconso;
    public Integer ismixed;
    public Integer isActive;
    public float tarifsmsnat;
    public float tarifappelnat;
    public float tarifdata;
    public float tarifappelfamille;
    public float tarifappelorange;
    public float tarifappelinternational;
    public float tarifsmsfamille;
    public float tarifsmsorange;
    public float tarifsmsinternational;
    public String groups1;
    public Integer groupsrank1;
    public String groups2;
    public Integer groupsrank2;
    public String groups3;
    public Integer groupsrank3;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getIsroaming() {
        return isroaming;
    }

    public void setIsroaming(Integer isroaming) {
        this.isroaming = isroaming;
    }

    public Integer getIstoexcludefrominfoconso() {
        return istoexcludefrominfoconso;
    }

    public void setIstoexcludefrominfoconso(Integer istoexcludefrominfoconso) {
        this.istoexcludefrominfoconso = istoexcludefrominfoconso;
    }

    public Integer getIsmixed() {
        return ismixed;
    }

    public void setIsmixed(Integer ismixed) {
        this.ismixed = ismixed;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public float getTarifsmsnat() {
        return tarifsmsnat;
    }

    public void setTarifsmsnat(float tarifsmsnat) {
        this.tarifsmsnat = tarifsmsnat;
    }

    public float getTarifappelnat() {
        return tarifappelnat;
    }

    public void setTarifappelnat(float tarifappelnat) {
        this.tarifappelnat = tarifappelnat;
    }

    public float getTarifdata() {
        return tarifdata;
    }

    public void setTarifdata(float tarifdata) {
        this.tarifdata = tarifdata;
    }

    public float getTarifappelfamille() {
        return tarifappelfamille;
    }

    public void setTarifappelfamille(float tarifappelfamille) {
        this.tarifappelfamille = tarifappelfamille;
    }

    public float getTarifappelorange() {
        return tarifappelorange;
    }

    public void setTarifappelorange(float tarifappelorange) {
        this.tarifappelorange = tarifappelorange;
    }

    public float getTarifappelinternational() {
        return tarifappelinternational;
    }

    public void setTarifappelinternational(float tarifappelinternational) {
        this.tarifappelinternational = tarifappelinternational;
    }

    public float getTarifsmsfamille() {
        return tarifsmsfamille;
    }

    public void setTarifsmsfamille(float tarifsmsfamille) {
        this.tarifsmsfamille = tarifsmsfamille;
    }

    public float getTarifsmsorange() {
        return tarifsmsorange;
    }

    public void setTarifsmsorange(float tarifsmsorange) {
        this.tarifsmsorange = tarifsmsorange;
    }

    public float getTarifsmsinternational() {
        return tarifsmsinternational;
    }

    public void setTarifsmsinternational(float tarifsmsinternational) {
        this.tarifsmsinternational = tarifsmsinternational;
    }

    public String getGroups1() {
        return groups1;
    }

    public void setGroups1(String groups1) {
        this.groups1 = groups1;
    }

    public Integer getGroupsrank1() {
        return groupsrank1;
    }

    public void setGroupsrank1(Integer groupsrank1) {
        this.groupsrank1 = groupsrank1;
    }

    public String getGroups2() {
        return groups2;
    }

    public void setGroups2(String groups2) {
        this.groups2 = groups2;
    }

    public Integer getGroupsrank2() {
        return groupsrank2;
    }

    public void setGroupsrank2(Integer groupsrank2) {
        this.groupsrank2 = groupsrank2;
    }

    public String getGroups3() {
        return groups3;
    }

    public void setGroups3(String groups3) {
        this.groups3 = groups3;
    }

    public Integer getGroupsrank3() {
        return groupsrank3;
    }

    public void setGroupsrank3(Integer groupsrank3) {
        this.groupsrank3 = groupsrank3;
    }
}
